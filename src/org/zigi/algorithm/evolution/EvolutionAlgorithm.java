package org.zigi.algorithm.evolution;

import java.security.SecureRandom;
import java.util.LinkedList;
import java.util.List;

import org.zigi.algorithm.evolution.individual.Individual;
import org.zigi.algorithm.problem.Problem;
import org.zigi.programming.listener.StateListener;

public abstract class EvolutionAlgorithm<T extends Problem> implements Runnable {
	private Population population = new Population();
	private T problem;
	private EvolutionAlgorithmState currentState;

	/* GENERATION */
	private int currGeneration = 1;
	private int maxGeneration = 10;

	/* RUNNABLE */
	private boolean isRunning = false;
	private Thread thread;

	/* LISTENERS */
	private List<StateListener<EvolutionAlgorithmState>> stateListener = new LinkedList<StateListener<EvolutionAlgorithmState>>();
	protected static SecureRandom random = new SecureRandom();

	/* SOLUTIONS */
	private Individual bestSolution;

	/* POPULATION */
	public Population getPopulation() {
		return population;
	}

	public void setPopulation(Population population) {
		this.population = population;
	}

	/* PROBLEMS */
	public T getProblem() {
		return problem;
	}

	public void setProblem(T problem) {
		this.problem = problem;
	}

	/* THREAD */
	public void start() {
		thread = new Thread(this);
		thread.start();
	}

	public void stop() {
		isRunning = false;
		currGeneration = 0;
	}

	/* MAX GENERATIONS */
	public int getMaxGeneration() {
		return maxGeneration;
	}

	public void setMaxGeneration(int maxGeneration) {
		this.maxGeneration = maxGeneration;
	}

	/* GENERATIONS */
	public int getCurrGeneration() {
		return currGeneration;
	}

	public void setCurrGeneration(int generation) {
		this.currGeneration = generation;
	}

	public void incrementGeneration() {
		this.currGeneration++;
	}

	/* BEST SOLUTION */
	public void setBestSolution(Individual best) {
		try {
			this.bestSolution = best;
			fireStateListener(EvolutionAlgorithmState.NEW_BEST_SOLUTION_STATE);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Individual getBestSolution() {
		return bestSolution;
	}

	protected void refreshBest(Individual solution) {
		double fitnessVal = getProblem().getFitnessValue(solution);
		System.out.println("New best solution: " + fitnessVal + " (" + solution
				+ ") in " + getCurrGeneration());
		if (fitnessVal >= Double.MAX_VALUE)
			stopRunning();

		if (fitnessVal < getProblem().getFitnessValue(getBestSolution())) {
			setBestSolution(solution);
		}
	}

	/* ALGORITHM STATE */
	public EvolutionAlgorithmState getCurrentState() {
		return currentState;
	}

	private void setCurrentState(EvolutionAlgorithmState currentState) {
		this.currentState = currentState;
	}

	/* ALGORITHM */
	public abstract void run();

	/* LISTENERS */
	public void addStateListener(StateListener<EvolutionAlgorithmState> listener) {
		if (listener != null && !stateListener.contains(listener))
			stateListener.add(listener);
	}

	public void removeStateListener(
			StateListener<EvolutionAlgorithmState> listener) {
		if (listener != null && stateListener.contains(listener))
			stateListener.remove(listener);
	}

	public void clearStateListener() {
		stateListener.clear();
	}

	protected void fireStateListener(final EvolutionAlgorithmState state) {
		this.currentState = state;
		for (StateListener<EvolutionAlgorithmState> item : stateListener)
			item.handleStateChanged(this, state);
	}

	/* RUNNING */
	public boolean isRunning() {
		return isRunning;
	}

	public void startRunning() {
		isRunning = true;
	}

	public void stopRunning() {
		isRunning = false;
	}
}
