package org.zigi.algorithm.evolution;

public class EvolutionException extends Exception {
	private EvolutionAlgorithm algorithm;
	private EvolutionAlgorithmState state;

	public EvolutionException(String name, EvolutionAlgorithm source,
			EvolutionAlgorithmState state) {
		super(name);
		this.algorithm = source;
		this.state = state;
	}

	public EvolutionAlgorithm getAlgorithm() {
		return algorithm;
	}

	public EvolutionAlgorithmState getState() {
		return state;
	}
}
