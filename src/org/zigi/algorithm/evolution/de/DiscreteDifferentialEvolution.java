package org.zigi.algorithm.evolution.de;

import java.util.LinkedList;
import java.util.List;

import org.zigi.algorithm.evolution.EvolutionAlgorithm;
import org.zigi.algorithm.evolution.Population;
import org.zigi.algorithm.evolution.individual.Individual;
import org.zigi.algorithm.evolution.individual.Parameters;
import org.zigi.algorithm.evolution.individual.ParametersException;
import org.zigi.algorithm.problem.PermutativeProblem;

public class DiscreteDifferentialEvolution extends
		EvolutionAlgorithm<PermutativeProblem> implements Runnable {
	public static final int POPULATION_MIN = 4;
	public static final int POPULATION_MAX = Integer.MAX_VALUE;
	public static final double MUTATION_MIN = 0;
	public static final double CROSS_MIN = 0;
	public static final double CROSS_MAX = 1;
	public static final int GENERATION_MIN = 1;
	public static final int GENERATION_MAX = Integer.MAX_VALUE;

	/* PARAMETERS */
	protected double mutationConstant = 0.6;
	protected double crossRange = 0.5;

	@Override
	public String toString() {
		return "Diskrétní diferenciální evoluce";
	}

	public Parameters forwardTransformation(Parameters pars) {
		Parameters par = new Parameters();
		for (Number num : pars) {
			par.add((num.intValue() * getProblem().getDimension() * 100) / 999.0);
		}
		return par;
	}

	public Parameters backwardTransformation(Parameters pars) {
		Parameters par = new Parameters();
		for (Number num : pars) {
			int result = (int) Math.round(((1 + num.doubleValue()) * 999.0)
					/ (getProblem().getDimension() * 100));
			par.add(result);
		}
		return par;
	}

	public Individual applyStrategy(Individual active, Individual ind1,
			Individual ind2, Individual ind3) throws ParametersException {
		Parameters act = forwardTransformation((Parameters) active);
		Parameters first = forwardTransformation((Parameters) ind1);
		Parameters second = forwardTransformation((Parameters) ind2);
		Parameters third = forwardTransformation((Parameters) ind3);

		// šumový vektor
		Parameters noiseVector = Parameters.add(third, Parameters.multiply(
				Parameters.difference(first, second), mutationConstant));

		Parameters tryVector = new Parameters();
		for (int index = 0; index < noiseVector.size(); index++) {
			if (random.nextDouble() < crossRange)
				tryVector.add(noiseVector.get(index));
			else
				tryVector.add(act.get(index));
		}

		return backwardTransformation(tryVector);
	}

	/* REPRODUCTION */
	protected List<Individual> reproduction(Population population,
			PermutativeProblem function) throws Exception {
		List<Individual> result = new LinkedList<Individual>();
		LinkedList<Individual> temp = new LinkedList<Individual>();
		temp.addAll(population);

		for (int i = 0; i < population.size(); i++) {
			Individual active = temp.remove(i);
			Individual first = temp.remove(random.nextInt(temp.size()));
			Individual second = temp.remove(random.nextInt(temp.size()));
			Individual third = temp.remove(random.nextInt(temp.size()));

			Individual back = applyStrategy(active, first, second, third);

			Individual recursive = recursiveMutation(back, active);

			double fitness1 = getProblem().getFitnessValue(recursive);
			double fitness2 = getProblem().getFitnessValue(active);
			if (fitness1 > fitness2)
				result.add(recursive);
			else
				result.add(active);

			temp.clear();
			temp.addAll(population);
		}
		return result;
	}

	public Individual recursiveMutation(Individual item, Individual previous) {
		Parameters active = (Parameters) item;
		Parameters prev = (Parameters) previous;

		if (!getProblem().isValid(active))
			return prev;
		return active;
	}

	@Override
	public void run() {
		startRunning();
		setCurrGeneration(0);
		try {
			fireStateListener(DifferentialEvolutionState.STARTED_STATE);
			setBestSolution(getPopulation().get(0));
			while (isRunning() && getCurrGeneration() < getMaxGeneration()) {
				fireStateListener(DifferentialEvolutionState.START_REPORDUCTION_STATE);

				List<Individual> newPopulation = reproduction(getPopulation(),
						getProblem());
				getPopulation().clear();
				getPopulation().addAll(newPopulation);
				fireStateListener(DifferentialEvolutionState.NEW_POPULATION_CREATED_STATE);

				// sorting from best to worst
				Population.sortPopulation(getPopulation(), getProblem());
				// best of population is compared from best of all and updated
				refreshBest((Parameters) getPopulation().get(0));
				// next generation increment
				incrementGeneration();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		fireStateListener(DifferentialEvolutionState.ENDED_STATE);
		// stop algorithm
		stopRunning();
	}

	protected void refreshBest(Parameters individual) {
		double fitnessVal = getProblem().getFitnessValue(individual);
		double lastBest = getProblem().getFitnessValue(getBestSolution());
		if (fitnessVal > lastBest) {
			setBestSolution(individual);
			fireStateListener(DifferentialEvolutionState.NEW_BEST_SOLUTION_STATE);
		}
	}

	public double getMutationConstant() {
		return mutationConstant;
	}

	public void setMutationConstant(double mutationConstant) {
		this.mutationConstant = mutationConstant;
	}

	public double getCrossRange() {
		return crossRange;
	}

	public void setCrossRange(double crossRange) {
		this.crossRange = crossRange;
	}
}
