package org.zigi.algorithm.evolution.de;

import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.zigi.algorithm.evolution.EvolutionAlgorithm;
import org.zigi.algorithm.evolution.Population;
import org.zigi.algorithm.evolution.individual.Individual;
import org.zigi.algorithm.evolution.individual.Parameters;
import org.zigi.algorithm.problem.NumberProblem;
import org.zigi.algorithm.problem.Problem;

public class DifferentialEvolution extends EvolutionAlgorithm<NumberProblem>
		implements Runnable {

	public static Logger logger = Logger.getLogger(DifferentialEvolution.class);

	public static final int POPULATION_MIN = 4;
	public static final int POPULATION_MAX = Integer.MAX_VALUE;
	public static final double MUTATION_MIN = 0;
	public static final double MUTATION_MAX = 2;
	public static final double CROSS_MIN = 0;
	public static final double CROSS_MAX = 1;
	public static final int GENERATION_MIN = 1;
	public static final int GENERATION_MAX = Integer.MAX_VALUE;

	/* PARAMETERS */
	protected double mutationConstant = 0.6;
	protected double crossRange = 0.5;

	/**
	 * Konstruktor
	 */
	public DifferentialEvolution() {

	}

	protected void initialize() throws Exception {
		getProblem().initialize();
	}

	@Override
	public String toString() {
		return "Diferenci�ln� evoluce";
	}

	public void run() {
		startRunning();
		setCurrGeneration(0);
		try {
			fireStateListener(DifferentialEvolutionState.STARTED_STATE);
			initialize();
			setBestSolution(getPopulation().get(0));
			while (isRunning() && getCurrGeneration() < getMaxGeneration()) {
				fireStateListener(DifferentialEvolutionState.START_REPORDUCTION_STATE);
				List<Individual> newPopulation = reproduction(getPopulation(),
						getProblem());
				getPopulation().clear();
				getPopulation().addAll(newPopulation);
				fireStateListener(DifferentialEvolutionState.NEW_POPULATION_CREATED_STATE);
				Population.sortPopulation(getPopulation(), getProblem());
				refreshBest((Parameters) getPopulation().get(0));
				incrementGeneration();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		fireStateListener(DifferentialEvolutionState.ENDED_STATE);
		stopRunning();
	}

	/**
	 * Aktualizuje globálního nejlepšího jedince a jeho fitness hodnotu
	 * 
	 * @param individual
	 */
	protected void refreshBest(Parameters individual) {
		double fitnessVal = getProblem().getFitnessValue(individual);
		double lastBest = getProblem().getFitnessValue(getBestSolution());
		if (fitnessVal > lastBest) {
			setBestSolution(individual);
			fireStateListener(DifferentialEvolutionState.NEW_BEST_SOLUTION_STATE);
		}
	}

	/* REPRODUCTION */
	protected List<Individual> reproduction(Population population,
			Problem function) throws Exception {
		List<Individual> result = new LinkedList<Individual>();
		LinkedList<Individual> temp = new LinkedList<Individual>();
		temp.addAll(population);

		for (int i = 0; i < population.size(); i++) {
			Parameters active = (Parameters) temp.remove(i);
			Parameters first = (Parameters) temp.remove(random.nextInt(temp
					.size()));
			Parameters second = (Parameters) temp.remove(random.nextInt(temp
					.size()));
			Parameters third = (Parameters) temp.remove(random.nextInt(temp
					.size()));

			// šumový vektor
			Parameters noiseVector = Parameters.add(third, Parameters.multiply(
					Parameters.difference(first, second), mutationConstant));
			Parameters tryVector = new Parameters();
			for (int index = 0; index < noiseVector.size(); index++) {
				if (random.nextDouble() < crossRange)
					tryVector.add(noiseVector.get(index));
				else
					tryVector.add(active.get(index));
			}
			tryVector.correctParameters(getProblem());
			active.correctParameters(getProblem());
			double fitnessTry = function.getFitnessValue(tryVector);
			double fitnessActive = function.getFitnessValue(active);
			if (fitnessTry > fitnessActive) {
				result.add(tryVector);
			} else {
				result.add(active);
			}
			temp.clear();
			temp.addAll(population);
		}
		return result;
	}

	/* MUTATION FACTOR */
	public double getMutationConstant() {
		return mutationConstant;
	}

	public void setMutationConstant(double mutationConstant) {
		this.mutationConstant = mutationConstant;
	}

	/* CROSS RANGE */
	public double getCrossRange() {
		return crossRange;
	}

	public void setCrossRange(double crossRange) {
		this.crossRange = crossRange;
	}
}
