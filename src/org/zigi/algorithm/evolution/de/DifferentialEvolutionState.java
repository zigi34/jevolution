package org.zigi.algorithm.evolution.de;

import org.zigi.algorithm.evolution.EvolutionAlgorithmState;

public class DifferentialEvolutionState extends EvolutionAlgorithmState {

	private static final int START_REPRODUCTION = 845;

	public static DifferentialEvolutionState START_REPORDUCTION_STATE = new DifferentialEvolutionState(
			START_REPRODUCTION);

	private DifferentialEvolutionState() {

	}

	private DifferentialEvolutionState(int state) {
		super(state);
	}
}
