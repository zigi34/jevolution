package org.zigi.algorithm.evolution.de;

import java.util.LinkedList;
import java.util.List;

import org.zigi.algorithm.evolution.EvolutionAlgorithm;
import org.zigi.algorithm.evolution.Population;
import org.zigi.algorithm.evolution.individual.Individual;
import org.zigi.algorithm.evolution.individual.Parameters;
import org.zigi.algorithm.evolution.individual.ParametersException;
import org.zigi.algorithm.problem.PermutativeProblem;

public class EnhancedDifferentialEvolution extends
		EvolutionAlgorithm<PermutativeProblem> implements Runnable {

	/**
	 * Minimal value of individuals in population
	 */
	public static final int POPULATION_MIN = 4;
	/**
	 * Maximum value of individuals in population
	 */
	public static final int POPULATION_MAX = Integer.MAX_VALUE;
	/**
	 * Minimal value scale factor
	 */
	public static final double MUTATION_MIN = 0;
	/**
	 * Minimal value for CR
	 */
	public static final double CROSS_MIN = 0;
	/**
	 * Maximal value for CR
	 */
	public static final double CROSS_MAX = 1;
	/**
	 * Minimal generation Gmax
	 */
	public static final int GENERATION_MIN = 1;
	/**
	 * Maximum generation Gmax
	 */
	public static final int GENERATION_MAX = Integer.MAX_VALUE;

	/* PARAMETERS */
	/**
	 * Scale factor value
	 */
	protected double scaleFactor = 0.2;
	/**
	 * Crossrange value
	 */
	protected double crossRange = 0.5;

	@Override
	public String toString() {
		return "Enhanced Differential Evolution";
	}

	/**
	 * Identify infeasible values in solution
	 * 
	 * @param dimension
	 *            dimension size (jobs)
	 * @param solution
	 *            solution to be repairment
	 * @param violate
	 *            list of indexes, which violate solution
	 * @param missing
	 *            list of missing values in permutation
	 */
	public void repairment(int dimension, Parameters solution,
			List<Integer> violate, List<Integer> missing) {

		int Counter = 0;
		for (int i = 0; i < dimension; i++) {
			for (int j = 0; j < dimension; j++) {
				if (i == solution.get(j).intValue()) {
					Counter++;
				}
			}
			if (Counter > 1) {
				int Index = 0;
				for (int j = 0; j < dimension; j++) {
					if (i == solution.get(j).intValue()) {
						Index++;
						if (Index > 1) {
							violate.add(j);
						}
					}
				}
			}
			if (Counter == 0) {
				missing.add(i);
			}
			Counter = 0;
		}
	}

	/**
	 * Repair mechanism to guarantee moving values out of bounds
	 * 
	 * @param ind
	 *            permutation (solution)
	 */
	public void repairBoundary(Individual ind) {
		Parameters pars = (Parameters) ind;
		for (int i = 0; i < pars.size(); i++) {
			int num = pars.get(i).intValue();
			if (num > getProblem().getHi())
				pars.set(i, getProblem().getHi());
			else if (num < getProblem().getLo())
				pars.set(i, getProblem().getLo());
		}
	}

	/**
	 * Repair value violate feasible solution start from left
	 * 
	 * @param solution
	 *            repaired solution
	 * @param violate
	 *            list of violate values
	 * @param missing
	 *            missing values
	 */
	public void frontMutation(Parameters solution, List<Integer> violate,
			List<Integer> missing) {
		for (int i = 0; i < violate.size(); i++) {
			int randomIndex = random.nextInt(missing.size());
			solution.set(violate.get(i), missing.get(randomIndex));
			missing.remove(randomIndex);
		}
	}

	/**
	 * Repair value violate feasible solution start from right
	 * 
	 * @param solution
	 *            repaired solution
	 * @param violate
	 *            list of violate values
	 * @param missing
	 *            missing values
	 */
	public void backMutation(Parameters solution, List<Integer> violate,
			List<Integer> missing) {
		for (int i = violate.size() - 1; i >= 0; i--) {
			int randomIndex = random.nextInt(missing.size());
			solution.set(violate.get(i), missing.get(randomIndex));
			missing.remove(randomIndex);
		}
	}

	/**
	 * Repair value violate feasible solution with randomly selected violate
	 * values
	 * 
	 * @param solution
	 *            repaired solution
	 * @param violate
	 *            list of violate values
	 * @param missing
	 *            missing values
	 */
	public void randomMutation(Parameters solution, List<Integer> violate,
			List<Integer> missing) {
		for (int i = violate.size() - 1; i > 0; i--) {
			int violateRand = random.nextInt(violate.size());
			int missingRand = random.nextInt(missing.size());
			solution.set(violate.get(violateRand), missing.get(missingRand));
			missing.remove(missingRand);
			violate.remove(violateRand);
		}
	}

	/**
	 * Mutation mechanism which swap randomly selected values
	 * 
	 * @param solution
	 *            permutation
	 * @return mutated permutation
	 * @throws Exception
	 */
	public Parameters standardMutation(Parameters solution) throws Exception {
		Parameters pr = solution.createCopy();
		int rand1 = random.nextInt(pr.size());
		int rand2 = random.nextInt(pr.size());

		int temp = pr.get(rand2).intValue();
		pr.set(rand2, pr.get(rand1));
		pr.set(rand1, temp);
		return pr;
	}

	/**
	 * Mutate mechanism which shift sequence of values from start index to end
	 * index to one position to left
	 * 
	 * @param solution
	 *            permuation
	 * @return shifted permutation
	 * @throws Exception
	 */
	public Parameters insertMutation(Parameters solution) throws Exception {
		Parameters par = solution.createCopy();
		int rand1 = random.nextInt(par.size());
		int rand2 = random.nextInt(par.size());

		if (rand1 > rand2) {
			int temp = rand1;
			rand1 = rand2;
			rand2 = temp;
		}

		int temp = par.get(rand1).intValue();
		for (int i = rand1; i < rand2; i++) {
			par.set(i, par.get(i + 1));
		}
		par.set(rand2, temp);
		return par;
	}

	/**
	 * Forward transformation
	 * 
	 * @param pars
	 * @return
	 */
	public Parameters forwardTransformation(Parameters pars) {
		Parameters par = new Parameters();
		for (Number num : pars) {
			par.add((num.intValue() * getProblem().getDimension() * 100) / 999.0);
		}
		return par;
	}

	/**
	 * Backward transformation
	 * 
	 * @param pars
	 * @return
	 */
	public Parameters backwardTransformation(Parameters pars) {
		Parameters par = new Parameters();
		for (Number num : pars) {
			int result = (int) Math.round(((1 + num.doubleValue()) * 999.0)
					/ (getProblem().getDimension() * 100));
			par.add(result);
		}
		return par;
	}

	/**
	 * Local search
	 * 
	 * @param pars
	 */
	public void localSearch(Parameters pars) {
		double timespan = getProblem().getSolutionValue(pars);
		for (int i = 1; i < pars.size() - 1; i++) {
			for (int j = i + 1; j < pars.size(); j++) {
				// swap values
				int temp = pars.get(i).intValue();
				pars.set(i, pars.get(j).intValue());
				pars.set(j, temp);

				// compare new timespan with old timespan
				double newTimespan = getProblem().getSolutionValue(pars);
				// if it is better, than stop swapping
				if (newTimespan < timespan)
					return;

				// return back
				temp = pars.get(i).intValue();
				pars.set(i, pars.get(j).intValue());
				pars.set(j, temp);
			}
		}
	}

	public Individual applyStrategy(Individual active, Individual ind1,
			Individual ind2, Individual ind3) throws ParametersException {
		Parameters act = forwardTransformation((Parameters) active);
		Parameters first = forwardTransformation((Parameters) ind1);
		Parameters second = forwardTransformation((Parameters) ind2);
		Parameters third = forwardTransformation((Parameters) ind3);

		// šumový vektor
		Parameters noiseVector = Parameters.add(third, Parameters.multiply(
				Parameters.difference(first, second), scaleFactor));

		Parameters tryVector = new Parameters();
		for (int index = 0; index < noiseVector.size(); index++) {
			if (random.nextDouble() < crossRange)
				tryVector.add(noiseVector.get(index));
			else
				tryVector.add(act.get(index));
		}

		return backwardTransformation(tryVector);
	}

	/**
	 * Reproduction with mutation strategy of DE.
	 * 
	 * @param population
	 * @param function
	 * @return
	 * @throws Exception
	 */
	protected List<Individual> reproduction(Population population,
			PermutativeProblem function) throws Exception {
		List<Individual> result = new LinkedList<Individual>();
		LinkedList<Individual> temp = new LinkedList<Individual>();
		temp.addAll(population);

		for (int i = 0; i < population.size(); i++) {
			Individual active = temp.remove(i);
			Individual first = temp.remove(random.nextInt(temp.size()));
			Individual second = temp.remove(random.nextInt(temp.size()));
			Individual third = temp.remove(random.nextInt(temp.size()));

			Individual back = applyStrategy(active, first, second, third);
			repairBoundary(back);
			List<Integer> violate = new LinkedList<Integer>();
			List<Integer> missing = new LinkedList<Integer>();
			repairment(getProblem().getDimension(), (Parameters) back, violate,
					missing);
			randomMutation((Parameters) back, violate, missing);
			result.add(back);

			temp.clear();
			temp.addAll(population);
		}
		return result;
	}

	@Override
	public void run() {
		startRunning();
		setCurrGeneration(0);
		int newBest = 0;
		try {
			fireStateListener(DifferentialEvolutionState.STARTED_STATE);
			setBestSolution(getPopulation().get(0));
			while (isRunning() && getCurrGeneration() < getMaxGeneration()) {
				fireStateListener(DifferentialEvolutionState.START_REPORDUCTION_STATE);

				List<Individual> newPopulation = reproduction(getPopulation(),
						getProblem());

				// Replace with better mutated solution (standard mutation)
				for (int i = 0; i < getPopulation().size(); i++) {
					Parameters newPar = standardMutation((Parameters) newPopulation
							.get(i));
					double fitness1 = getProblem().getFitnessValue(newPar);
					double fitness2 = getProblem().getFitnessValue(
							newPopulation.get(i));
					if (fitness1 > fitness2)
						newPopulation.set(i, newPar);
				}

				// Replace with better mutated solution (insertion)
				for (int i = 0; i < getPopulation().size(); i++) {
					Parameters newPar = insertMutation((Parameters) newPopulation
							.get(i));
					double fitness1 = getProblem().getFitnessValue(newPar);
					double fitness2 = getProblem().getFitnessValue(
							newPopulation.get(i));
					if (fitness1 > fitness2)
						newPopulation.set(i, newPar);
				}

				getPopulation().clear();
				getPopulation().addAll(newPopulation);
				fireStateListener(DifferentialEvolutionState.NEW_POPULATION_CREATED_STATE);

				// sorting from best to worst
				Population.sortPopulation(getPopulation(), getProblem());
				// best of population is compared from best of all and updated
				if (!refreshBest((Parameters) getPopulation().get(0))) {
					newBest++;
				}
				// stagnate for 5 generation, than local search for echa
				// individual
				if (newBest >= 5) {
					newBest = 0;
					for (Individual ind : getPopulation()) {
						Parameters par = (Parameters) ind;
						localSearch(par);
					}
				}
				// next generation increment
				incrementGeneration();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		fireStateListener(DifferentialEvolutionState.ENDED_STATE);
		// stop algorithm
		stopRunning();
	}

	protected boolean refreshBest(Parameters individual) {
		double fitnessVal = getProblem().getFitnessValue(individual);
		double lastBest = getProblem().getFitnessValue(getBestSolution());
		if (fitnessVal > lastBest) {
			setBestSolution(individual);
			fireStateListener(DifferentialEvolutionState.NEW_BEST_SOLUTION_STATE);
			return true;
		}
		return false;
	}

	public double getMutationConstant() {
		return scaleFactor;
	}

	public void setMutationConstant(double mutationConstant) {
		this.scaleFactor = mutationConstant;
	}

	public double getCrossRange() {
		return crossRange;
	}

	public void setCrossRange(double crossRange) {
		this.crossRange = crossRange;
	}
}
