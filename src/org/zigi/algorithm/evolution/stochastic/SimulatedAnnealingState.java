package org.zigi.algorithm.evolution.stochastic;

import org.zigi.algorithm.evolution.EvolutionAlgorithmState;

public class SimulatedAnnealingState extends EvolutionAlgorithmState {

	private static final int STARTED = 1;
	private static final int ENDED = 2;
	private static final int NEW_BEST_SOLUTION = 3;
	private static final int SET_ACTUAL_INDIVIDUAL = 4;

	public static EvolutionAlgorithmState STARTED_STATE = new SimulatedAnnealingState(
			STARTED);
	public static EvolutionAlgorithmState ENDED_STATE = new SimulatedAnnealingState(
			ENDED);
	public static EvolutionAlgorithmState NEW_BEST_SOLUTION_STATE = new SimulatedAnnealingState(
			NEW_BEST_SOLUTION);
	public static EvolutionAlgorithmState SET_ACTUAL_INDIVIDUAL_STATE = new SimulatedAnnealingState(
			SET_ACTUAL_INDIVIDUAL);

	public int state;

	private SimulatedAnnealingState() {
	}

	private SimulatedAnnealingState(int state) {
		this.state = state;
	}
}
