package org.zigi.algorithm.evolution.stochastic;

import java.util.LinkedList;
import java.util.List;

import org.zigi.algorithm.evolution.EvolutionAlgorithm;
import org.zigi.algorithm.evolution.EvolutionAlgorithmState;
import org.zigi.algorithm.evolution.VariableRange;
import org.zigi.algorithm.evolution.individual.Parameters;
import org.zigi.algorithm.problem.NumberProblem;

public class SimulatedAnnealing extends EvolutionAlgorithm<NumberProblem> {
	private Parameters actualParameters;

	private double rangeStart = 3;
	private double rangeStop = 0.01;
	private double multiplier = 0.75;
	private double actualRange = rangeStart;
	private int repeated = 3;

	/**
	 * Konstruktor
	 */
	public SimulatedAnnealing() {
	}

	@Override
	public String toString() {
		return "Simulované žíhání";
	}

	private boolean decreaseRange() {
		actualRange = actualRange * multiplier;
		if (actualRange > rangeStop)
			return true;
		return false;
	}

	private Parameters metropolisAlgorithm(Parameters xInit, double tempRange) {
		Parameters x = xInit;
		for (int i = 0; i < repeated; i++) {
			List<VariableRange<Number>> ranges = new LinkedList<VariableRange<Number>>();
			for (int j = 0; j < x.size(); j++) {
				VariableRange<Number> r = new VariableRange<Number>(0.0,
						tempRange);
				r.setCenterValue(x.get(j).doubleValue());
				r.setRange(tempRange);
				ranges.add(r);
			}

			Parameters xStripe = Parameters.getRandomParameters(ranges);
			if (getProblem().getFitnessValue(xStripe) < getProblem()
					.getFitnessValue(getBestSolution()))
				setBestSolution(xStripe);

			double prev = getProblem().getFunctionValue(x);
			double next = getProblem().getFunctionValue(xStripe);

			double pr = Math.min(1.0, Math.exp(-(getProblem().getFunctionValue(
					xStripe) - getProblem().getFunctionValue(x))
					/ tempRange));
			if (pr > random.nextDouble()) {
				x = xStripe;
			}
		}
		return x;
	}

	public void run() {
		startRunning();
		setCurrGeneration(0);
		actualRange = getRangeStart();
		fireStateListener(SimulatedAnnealingState.STARTED_STATE);

		setBestSolution(getPopulation().get(0));
		do {
			for (int i = 0; i < getPopulation().size(); i++) {
				Parameters xOut = metropolisAlgorithm(
						(Parameters) getPopulation().get(i), actualRange);
				getPopulation().set(i, xOut);
			}
			incrementGeneration();
			fireStateListener(EvolutionAlgorithmState.NEW_POPULATION_CREATED_STATE);
		} while (decreaseRange());
		fireStateListener(SimulatedAnnealingState.ENDED_STATE);
		stopRunning();
	}

	public Parameters getActualParameters() {
		return this.actualParameters;
	}

	public void setActualParameters(Parameters actual) {
		this.actualParameters = actual;
		fireStateListener(SimulatedAnnealingState.SET_ACTUAL_INDIVIDUAL_STATE);
	}

	/* REPEAT COUNT = POPULATION SIZE */
	public double getRangeStart() {
		return rangeStart;
	}

	public void setRangeStart(double rangeStart) {
		this.rangeStart = rangeStart;
	}

	public double getRangeStop() {
		return rangeStop;
	}

	public void setRangeStop(double rangeStop) {
		this.rangeStop = rangeStop;
	}

	public double getMultiplier() {
		return multiplier;
	}

	public void setMultiplier(double multiplier) {
		this.multiplier = multiplier;
	}

	public void setRepeated(int count) {
		this.repeated = count;
	}
}
