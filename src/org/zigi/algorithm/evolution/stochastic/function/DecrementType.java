package org.zigi.algorithm.evolution.stochastic.function;

public enum DecrementType {
	DECREMENT_COUNT, DECREMENT_SIZE
}
