package org.zigi.algorithm.evolution.stochastic.function;


public abstract class ReduceFunction {

	protected double startSize;
	protected double finishSize;
	protected double actualSize;

	public ReduceFunction(double start, double finish) {
		this.startSize = start;
		this.finishSize = finish;
	}

	public boolean isFinished() {
		if (actualSize > actualSize)
			return false;
		return true;
	}

	public double getActualSize() {
		return actualSize;
	}

	public abstract void reduce();
}
