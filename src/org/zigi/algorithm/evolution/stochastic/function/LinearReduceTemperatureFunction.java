package org.zigi.algorithm.evolution.stochastic.function;

public class LinearReduceTemperatureFunction extends ReduceFunction {

	private double decrementSize;
	private int decrementCount;
	private DecrementType decrementType = DecrementType.DECREMENT_COUNT;

	public LinearReduceTemperatureFunction(double start, double finish,
			int count) {
		super(start, finish);
		decrementCount = count;
		decrementType = DecrementType.DECREMENT_COUNT;
	}

	public LinearReduceTemperatureFunction(double start, double finish,
			double size) {
		super(start, finish);
		decrementSize = size;
		decrementType = DecrementType.DECREMENT_SIZE;
	}

	@Override
	public void reduce() {
		double decrementValue = 0;
		if (decrementType == DecrementType.DECREMENT_COUNT) {
			/* REDUCTION BY COUNT */
			decrementValue = (startSize - finishSize) / decrementCount;
		} else {
			/* REDUCTION BY SIZE */
			decrementValue = decrementSize;
		}
		actualSize = actualSize - decrementValue;
	}

	/** GETTERS AND SETTERS */
	public double getReduction() {
		return decrementSize;
	}

	public void setReduction(double reduction) {
		this.decrementSize = reduction;
	}

	public double getReductionSize() {
		return decrementSize;
	}

	public void setReductionSize(double reductionSize) {
		this.decrementSize = reductionSize;
		this.decrementType = DecrementType.DECREMENT_SIZE;
	}

	public int getReductionCount() {
		return decrementCount;
	}

	public void setReductionCount(int reductionCount) {
		this.decrementCount = reductionCount;
		this.decrementType = DecrementType.DECREMENT_COUNT;
	}

	public DecrementType getReductionType() {
		return decrementType;
	}

	public void setReductionType(DecrementType reductionType) {
		this.decrementType = reductionType;
	}
}
