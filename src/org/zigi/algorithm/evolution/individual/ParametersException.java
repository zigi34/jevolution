package org.zigi.algorithm.evolution.individual;



public class ParametersException extends Exception {
	/**
	 * VERSION ID
	 */
	private static final long serialVersionUID = -8257906265342850348L;
	private Parameters parm;

	public ParametersException(String message, Parameters par) {
		super(message);
		this.parm = par;
	}

	public Parameters getParm() {
		return parm;
	}
}
