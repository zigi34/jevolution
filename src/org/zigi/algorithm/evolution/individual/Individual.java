package org.zigi.algorithm.evolution.individual;

public abstract class Individual {
	public abstract Individual createCopy() throws Exception;

	public abstract int size();
}
