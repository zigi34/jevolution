package org.zigi.algorithm.evolution.individual;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.zigi.algorithm.evolution.gp.types.Expression;
import org.zigi.algorithm.evolution.gp.types.ITerminalExpression;
import org.zigi.algorithm.evolution.gp.types.NullExpression;
import org.zigi.algorithm.evolution.gp.types.VarExpr;

public class ExpressionTree extends Individual {
	private Expression root = new NullExpression();
	private int maxHeight = 0;
	private static Random random = new Random();

	public void mutate(List<Expression> terminals,
			List<Expression> neterminals, int maxDepth, boolean complete) {

		List<Expression> structures = new LinkedList<Expression>();
		structures.addAll(neterminals);
		structures.addAll(terminals);

		// N�hodn� vybereme v�raz, kter� ze stromu odstran�me spolu s potomky a
		// vlo��me m�sto n�j Null v�raz pro budov�n� nov�ho podstromu z tohoto
		// m�sta
		int treeSize = size();
		int randomIndex = random.nextInt(treeSize);
		Expression expression = get(randomIndex);
		// Pokud je n�hodn� vybr�n ro�en stromu
		if (expression.equals(getRoot())) {
			// pokud m��eme vybrat pouze root uzel, tak generujeme cel� strom
			// znovu
			setRoot(new NullExpression());
		} else {
			// pokud se jedn� o jin� uzel, ne� je ko�enov�, tak jej odstran�me a
			// nahrad�me Null uzlem
			expression.getParent().getChild().remove(expression);
			Expression newExpression = new NullExpression();
			newExpression.setParent(expression.getParent());
			expression.getParent().getChild().add(newExpression);
		}

		while (true) {
			// tree.addRandomExpression(structures, terminals);

			List<Expression> leaves = getNullExpressions();
			// is no leaves
			if (leaves.size() == 0)
				break; // than stop process of generating

			Expression expr = leaves.get(random.nextInt(leaves.size()));
			if (complete) {
				if (expr.getDepth() < maxDepth) {
					setExpression(
							neterminals.get(random.nextInt(neterminals.size()))
									.getInstance(), expr);
				} else {
					setExpression(
							terminals.get(random.nextInt(terminals.size()))
									.getInstance(), expr);
				}
			} else {
				if (expr.getDepth() >= maxDepth) {
					setExpression(
							terminals.get(random.nextInt(terminals.size()))
									.getInstance(), expr);
				} else
					setExpression(
							structures.get(random.nextInt(structures.size()))
									.getInstance(), expr);
			}
		}
	}

	public static ExpressionTree randomTree(List<Expression> structures,
			int maxDepth, boolean complete) {
		// First fill list with terminal symbols to ending growing of tree
		List<ITerminalExpression> terminals = new LinkedList<>();
		for (Expression structure : structures)
			if (structure.isTerminal())
				terminals.add((ITerminalExpression) structure);

		List<Expression> neterminals = new LinkedList<>();
		for (Expression structure : structures)
			if (!structure.isTerminal())
				neterminals.add(structure);

		// Creating new tree
		ExpressionTree tree = new ExpressionTree(maxDepth);

		// Creating first symbol as root of tree
		while (true) {
			// tree.addRandomExpression(structures, terminals);

			List<Expression> leaves = tree.getNullExpressions();
			// is no leaves
			if (leaves.size() == 0)
				break; // than stop process of generating

			Expression expr = leaves.get(random.nextInt(leaves.size()));
			if (complete) {
				if (expr.getDepth() < maxDepth) {
					tree.setExpression(
							neterminals.get(random.nextInt(neterminals.size()))
									.getInstance(), expr);
				} else {
					tree.setExpression(
							terminals.get(random.nextInt(terminals.size()))
									.getInstance(), expr);
				}
			} else {
				if (expr.getDepth() == maxDepth) {
					tree.setExpression(
							terminals.get(random.nextInt(terminals.size()))
									.getInstance(), expr);
				} else
					tree.setExpression(
							structures.get(random.nextInt(structures.size()))
									.getInstance(), expr);
			}
		}
		return tree;
	}

	public ExpressionTree(int maxHeight) {
		this.maxHeight = maxHeight;
	}

	public List<Expression> getLeaves() {
		List<Expression> leaves = new LinkedList<Expression>();
		if (root != null) {
			root.takeLeaves(leaves);
		}
		return leaves;
	}

	/**
	 * Return number representation of height of tree
	 * 
	 * @return
	 */
	public int getHeight() {
		int maxHeight = 0;
		List<Expression> leaves = getLeaves();
		for (Expression expr : leaves) {
			if (expr.getDepth() > maxHeight)
				maxHeight = expr.getDepth();
		}
		return maxHeight;
	}

	private void addRandomExpression(List<Expression> symbols,
			List<ITerminalExpression> terminals) {
		// Collecting leaves of tree (NullExpression) = adepts to new symbol
		List<Expression> leaves = getNullExpressions();
		// is no leaves
		if (leaves.size() == 0 || getHeight() == maxHeight)
			return; // than stop process of generating

		// Random generating first symbol to the tree as a root node
		Expression expr = leaves.get(random.nextInt(leaves.size()));
		setExpression(
				symbols.get(random.nextInt(symbols.size())).getInstance(), expr);
	}

	public void setExpression(Expression what, Expression instead) {
		if (instead.equals(root)) {
			root = what;
		} else {
			int index = instead.getParent().getChild().indexOf(instead);
			what.setParent(instead.getParent());
			instead.getParent().getChild().set(index, what);
		}
	}

	public List<Integer> getDepths() {
		List<Integer> depths = new LinkedList<Integer>();
		List<Expression> leaves = getLeaves();
		for (Expression expr : leaves) {
			depths.add(expr.getDepth());
		}
		return depths;
	}

	@Override
	public String toString() {
		return root.toString();
	}

	public Double getResult() {
		return root.getResult();
	}

	public List<Expression> getNullExpressions() {
		List<Expression> leaves = new LinkedList<Expression>();
		if (root != null) {
			if (root instanceof NullExpression)
				leaves.add(root);
			root.takeNullExpressions(leaves);
		}
		return leaves;
	}

	public List<Expression> getExpressions() {
		List<Expression> expressions = new LinkedList<Expression>();
		if (root != null) {
			root.takeExpressions(expressions);
		}
		expressions.add(root);
		return expressions;
	}

	public int getMaxHeight() {
		return maxHeight;
	}

	public void setRoot(Expression expr) {
		this.root = expr;
	}

	public Expression getRoot() {
		return this.root;
	}

	@Override
	public Individual createCopy() throws Exception {
		ExpressionTree tree = new ExpressionTree(getHeight());
		Expression rootExpr = getRoot().createCopy();
		tree.setRoot(rootExpr);
		return tree;
	}

	@Override
	public int size() {
		return size(root) + 1;
	}

	private int size(Expression express) {
		int prevSize = 0;
		for (Expression expr : express.getChild()) {
			prevSize += 1 + size(expr);
		}
		return prevSize;
	}

	public Expression get(int index) {
		List<Expression> list = getExpressions();
		return list.get(index);
	}

	public String getPrefixMode() {
		return getRoot().prefixMode();
	}

	/**
	 * This method return all variable instances of the tree.
	 * 
	 * @return all variables in tree
	 */
	public List<VarExpr> getVarExpressions() {
		List<VarExpr> resultList = new LinkedList<VarExpr>();
		for (Expression expression : getExpressions()) {
			if (expression instanceof VarExpr) {
				resultList.add((VarExpr) expression);
			}
		}
		return resultList;
	}
}
