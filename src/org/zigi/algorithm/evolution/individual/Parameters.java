package org.zigi.algorithm.evolution.individual;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import org.zigi.algorithm.evolution.VariableRange;
import org.zigi.algorithm.evolution.util.Constants;
import org.zigi.algorithm.problem.NumberProblem;

public class Parameters extends Individual implements List<Number>,
		Collection<Number> {
	private List<Number> params = new LinkedList<Number>();

	/* VECTOR OPERATIONS */
	/**
	 * Add two parameters
	 * 
	 * @param p1
	 *            parameter 1
	 * @param p2
	 *            parameter 2
	 * @return added parameter
	 * @throws ParametersException
	 *             exception if is not same size of parameters
	 */
	public static Parameters add(Parameters p1, Parameters p2)
			throws ParametersException {
		if (p1.size() != p2.size())
			throw new ParametersException(
					Constants.EXCEPTION_PARAMETERS_NOT_SAME_SIZE, p1);

		Parameters result = new Parameters();
		for (int i = 0; i < p1.size(); i++) {
			if (p1.get(i) instanceof Float || p1.get(i) instanceof Double) {
				result.add(p1.get(i).doubleValue() + p2.get(i).doubleValue());
			} else if (p1.get(i) instanceof Byte
					|| p1.get(i) instanceof Integer
					|| p1.get(i) instanceof Short || p1.get(i) instanceof Long) {
				result.add(p1.get(i).longValue() + p2.get(i).longValue());
			}
		}
		return result;
	}

	public static Parameters difference(Parameters p1, Parameters p2)
			throws ParametersException {
		if (p1.size() != p2.size())
			throw new ParametersException(
					Constants.EXCEPTION_PARAMETERS_NOT_SAME_SIZE, p1);

		Parameters result = new Parameters();
		for (int i = 0; i < p1.size(); i++) {
			if (p1.get(i) instanceof Float || p1.get(i) instanceof Double) {
				result.add(p1.get(i).doubleValue() - p2.get(i).doubleValue());
			} else if (p1.get(i) instanceof Byte
					|| p1.get(i) instanceof Integer
					|| p1.get(i) instanceof Short || p1.get(i) instanceof Long) {
				result.add(p1.get(i).longValue() - p2.get(i).longValue());
			}
		}
		return result;
	}

	public static Parameters multiply(Parameters p1, double multiplicator) {
		Parameters result = new Parameters();
		for (int i = 0; i < p1.size(); i++) {
			if (p1.get(i) instanceof Float || p1.get(i) instanceof Double) {
				result.add(p1.get(i).doubleValue() * multiplicator);
			} else if (p1.get(i) instanceof Byte
					|| p1.get(i) instanceof Integer
					|| p1.get(i) instanceof Short || p1.get(i) instanceof Long) {
				result.add(p1.get(i).longValue()
						* new Double(multiplicator).longValue());
			}
		}
		return result;
	}

	/* RANDOM PARAMETERS */
	public static Parameters getRandomParameters(
			List<VariableRange<Number>> constraints) {
		Parameters parms = new Parameters();
		for (VariableRange<Number> range : constraints) {
			parms.add(range.getRandomNumber());
		}
		return parms;
	}

	/* OVERRIDE */
	public boolean equals(Object o) {
		if (o instanceof Parameters) {
			Parameters pars = (Parameters) o;
			boolean res = true;
			for (int i = 0; i < pars.size() && i < params.size(); i++) {
				res = res & get(i).equals(pars.get(i));
			}
			return res;
		}
		return false;
	};

	/* COLLECTION IMPLEMENTS METHOD */
	@Override
	public int size() {
		return params.size();
	}

	@Override
	public boolean isEmpty() {
		return params.isEmpty();
	}

	@Override
	public boolean contains(Object o) {
		return params.contains(o);
	}

	@Override
	public Iterator<Number> iterator() {
		return params.iterator();
	}

	@Override
	public Object[] toArray() {
		return params.toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		return params.toArray(a);
	}

	@Override
	public boolean add(Number e) {
		return params.add(e);
	}

	@Override
	public boolean remove(Object o) {
		return params.remove(o);
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return params.contains(c);
	}

	@Override
	public boolean addAll(Collection<? extends Number> c) {
		return params.addAll(c);
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		return params.removeAll(c);
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		return params.retainAll(c);
	}

	@Override
	public void clear() {
		params.clear();
	}

	@Override
	public boolean addAll(int index, Collection<? extends Number> c) {
		return params.addAll(index, c);
	}

	@Override
	public Number get(int index) {
		return params.get(index);
	}

	@Override
	public Number set(int index, Number element) {
		return params.set(index, element);
	}

	@Override
	public void add(int index, Number element) {
		params.add(index, element);
	}

	@Override
	public Number remove(int index) {
		return params.remove(index);
	}

	@Override
	public int indexOf(Object o) {
		return params.indexOf(o);
	}

	@Override
	public int lastIndexOf(Object o) {
		return params.lastIndexOf(o);
	}

	@Override
	public ListIterator<Number> listIterator() {
		return params.listIterator();
	}

	@Override
	public ListIterator<Number> listIterator(int index) {
		return params.listIterator(index);
	}

	@Override
	public List<Number> subList(int fromIndex, int toIndex) {
		return params.subList(fromIndex, toIndex);
	}

	public Parameters createCopy() throws Exception {
		Parameters result = new Parameters();
		for (Number num : params) {
			result.params.add(new Double(num.doubleValue()));
		}
		return result;
	}

	public static void correctParameters(Parameters par, NumberProblem problem) {
		// Parameters pr = new Parameters();
		for (int i = 0; i < par.size(); i++) {
			if (problem.getRanges().size() <= i) {
				// pr.add(par.get(i));
				continue;
			}

			VariableRange<Number> range = problem.getRanges().get(i);
			double modulo = 0.0;
			if (par.get(i).doubleValue() > range.getMaxValue().doubleValue()) {
				modulo = (par.get(i).doubleValue() - range.getMaxValue()
						.doubleValue()) % range.getDifference().doubleValue();
				par.set(i, range.getMinValue().doubleValue() + modulo);
			} else if (par.get(i).doubleValue() < range.getMinValue()
					.doubleValue()) {
				modulo = (par.get(i).doubleValue() - range.getMaxValue()
						.doubleValue()) % range.getDifference().doubleValue();
				par.set(i, range.getMaxValue().doubleValue() - modulo);
			}
		}
	}

	public void correctParameters(NumberProblem problem) {
		correctParameters(this, problem);
	}

	@Override
	public String toString() {
		StringBuilder bs = new StringBuilder();
		for (Number num : this) {
			bs.append(num.toString() + ", ");
		}
		return bs.toString();
	}
}
