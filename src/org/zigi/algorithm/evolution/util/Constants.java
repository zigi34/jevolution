package org.zigi.algorithm.evolution.util;

public final class Constants {
	/* EVOLUTION EXCEPTION MESSAGE */
	public static final String EXCEPTION_POPULATION_NOT_SET = "Population is not initialize!";

	/* PARAMTERES EXCEPTION MESSAGE */
	public static final String EXCEPTION_PARAMETERS_NOT_SAME_SIZE = "Parametry nemají stejnou délku!";
}
