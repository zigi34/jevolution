package org.zigi.algorithm.evolution.util;

import net.objecthunter.exp4j.Expression;

import org.zigi.algorithm.evolution.individual.Parameters;
import org.zigi.math.expression.ExpressionContainer;

public class ExpressionHelper {

	public static double evaluate(ExpressionContainer expr, Parameters ind) {
		Expression expression = expr.getExpression();
		for (int i = 0; i < ind.size() && i < expr.getVariables().length; i++) {
			expression.setVariable(expr.getVariables()[i], ind.get(i)
					.doubleValue());
		}
		return expression.evaluate();
	}

}
