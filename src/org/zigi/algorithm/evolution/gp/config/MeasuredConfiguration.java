package org.zigi.algorithm.evolution.gp.config;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.zigi.algorithm.evolution.gp.types.VarExpr;
import org.zigi.algorithm.evolution.individual.ExpressionTree;

public class MeasuredConfiguration {

	private List<VarExpr> variableList = new ArrayList<VarExpr>();
	private List<List<Double>> solutions = new LinkedList<List<Double>>();

	public void addVariable(VarExpr expression) {
		if (expression != null && !variableList.contains(expression)) {
			variableList.add(expression);
		}
	}

	public List<VarExpr> getVariableList() {
		return variableList;
	}

	public void clearVariables() {
		variableList.clear();
	}

	public void addsolution(List<Double> configuration, Double result) {
		if (variableList.size() == configuration.size()) {
			List<Double> solution = new ArrayList<Double>();
			solution.addAll(configuration);
			solution.add(result);

			solutions.add(solution);
		}
	}

	public double getDeviation(ExpressionTree tree) {
		double deviation = 0.0;
		List<VarExpr> variables = tree.getVarExpressions();
		// calculate deviation for each of solutions
		for (List<Double> solution : solutions) {
			// through all variables
			for (int index = 0; index < variableList.size(); index++) {
				// and for each variable set values in the tree
				for (VarExpr variable : variables) {
					if (variableList.get(index).equals(variable)) {
						variable.setValue(solution.get(index));
					}
				}
			}

			// for evaluated tree we get a result
			double evaluation = tree.getResult();
			// compare evaluation with required result value for solution
			deviation += Math.abs(evaluation
					- solution.get(variableList.size()));
		}
		return deviation;
	}
}
