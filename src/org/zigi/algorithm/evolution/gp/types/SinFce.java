package org.zigi.algorithm.evolution.gp.types;

public class SinFce extends Expression {

	public SinFce() {
		super(1);
		setName("sin()");
	}

	@Override
	public int getMaxChilds() {
		return 1;
	}

	@Override
	public Double getResult() {
		return Math.sin(child.get(0).getResult().doubleValue());
	}

	@Override
	public Expression getInstance() {
		return new SinFce();
	}

	@Override
	public String toString() {
		return "sin(" + getChild().get(0).toString() + ")";
	}

	@Override
	public String prefixMode() {
		// TODO Auto-generated method stub
		return null;
	}

}
