package org.zigi.algorithm.evolution.gp.types;

import java.util.List;

public interface ITerminalExpression {

	void setValue(Double value);

	Double getValue();

	void takeLeaves(List<Expression> leaves);

	Expression getInstance();
}
