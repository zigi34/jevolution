package org.zigi.algorithm.evolution.gp.types;

import java.util.List;

public class NullExpression extends Expression {

	public NullExpression() {
		super(0);
		setName("null");
	}

	@Override
	public int getMaxChilds() {
		return 0;
	}

	@Override
	public Double getResult() {
		return null;
	}

	@Override
	public void takeLeaves(List<Expression> leaves) {
		leaves.add(this);
	}

	@Override
	public void takeNullExpressions(List<Expression> expressions) {
		expressions.add(this);
	}

	@Override
	public Expression getInstance() {
		return new NullExpression();
	}

	@Override
	public Expression createCopy() {
		NullExpression item = new NullExpression();
		return item;
	}

	@Override
	public String prefixMode() {
		// TODO Auto-generated method stub
		return null;
	}
}
