package org.zigi.algorithm.evolution.gp.types;

public class CosFce extends Expression {

	public CosFce() {
		super(1);
		setName("cos()");
	}

	@Override
	public int getMaxChilds() {
		return 1;
	}

	@Override
	public Double getResult() {
		return Math.cos(child.get(0).getResult().doubleValue());
	}

	@Override
	public Expression getInstance() {
		return new CosFce();
	}

	@Override
	public String toString() {
		return "cos(" + getChild().get(0).toString() + ")";
	}

	@Override
	public String prefixMode() {
		return null;
	}
}
