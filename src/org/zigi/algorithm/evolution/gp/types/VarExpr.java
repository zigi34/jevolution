package org.zigi.algorithm.evolution.gp.types;

import java.util.List;

public class VarExpr extends Expression implements ITerminalExpression {

	private Double value;
	private Double minValue;
	private Double maxValue;

	public VarExpr(String name) {
		super(0);
		this.setName(name);
	}

	@Override
	public int getMaxChilds() {
		return 0;
	}

	@Override
	public Double getResult() {
		return getValue().doubleValue();
	}

	@Override
	public void takeLeaves(List<Expression> leaves) {
		leaves.add(this);
	}

	@Override
	public void setValue(Double value) {
		this.value = value;
	}

	@Override
	public Double getValue() {
		return value;
	}

	public Double getMinValue() {
		return minValue;
	}

	public void setMinValue(Double minValue) {
		this.minValue = minValue;
	}

	public Double getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(Double maxValue) {
		this.maxValue = maxValue;
	}

	@Override
	public Expression getInstance() {
		VarExpr expr = new VarExpr(getName());
		expr.setValue(getValue());
		expr.setMinValue(getMinValue());
		expr.setMaxValue(getMaxValue());
		return expr;
	}

	@Override
	public String toString() {
		return getName();
	}

	@Override
	public String prefixMode() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof VarExpr) {
			VarExpr expression = (VarExpr) obj;
			if (expression.getName().equals(getName()))
				return true;
		}
		return false;
	}
}
