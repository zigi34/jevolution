package org.zigi.algorithm.evolution.gp.types;

import java.util.List;

public class Constant extends Expression implements ITerminalExpression {
	private Double value;

	public Constant(Double value) {
		super(0);
		this.value = value;
	}

	@Override
	public int getMaxChilds() {
		return 0;
	}

	@Override
	public Double getResult() {
		return value;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	@Override
	public void takeLeaves(List<Expression> leaves) {
		leaves.add(this);
	}

	@Override
	public Expression getInstance() {
		return new Constant(getValue());
	}

	@Override
	public String toString() {
		return getValue().toString();
	}

	@Override
	public String prefixMode() {
		return String.valueOf(getValue());
	}
}
