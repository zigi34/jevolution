package org.zigi.algorithm.evolution.gp.types;

public class PlusFce extends Expression {

	public PlusFce() {
		super(2);
		setName("+");
	}

	@Override
	public int getMaxChilds() {
		return 2;
	}

	@Override
	public Double getResult() {
		return child.get(0).getResult() + child.get(1).getResult();
	}

	@Override
	public Expression getInstance() {
		return new PlusFce();
	}

	@Override
	public String toString() {
		return "(" + getChild().get(0).toString() + "+"
				+ getChild().get(1).toString() + ")";
	}

	@Override
	public String prefixMode() {
		// TODO Auto-generated method stub
		return null;
	}

}
