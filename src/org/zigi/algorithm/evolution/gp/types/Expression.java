package org.zigi.algorithm.evolution.gp.types;

import java.util.LinkedList;
import java.util.List;

public abstract class Expression {
	private String name;
	protected List<Expression> child = new LinkedList<Expression>();
	protected Expression parent;
	private int arita;
	private int childIndex = 0;

	public Expression(int childs) {
		this.arita = childs;
		for (int i = 0; i < childs; i++) {
			Expression expr = new NullExpression();
			expr.setParent(this);
			child.add(expr);
		}
	}

	public void add(Expression expr) {
		if (child.size() < arita)
			child.add(expr);
		else
			child.set(childIndex, expr);
		expr.setParent(this);
		childIndex = (childIndex + 1) % arita;
	}

	/**
	 * Return number information that this is terminal symbol, which is
	 * determined by number of maximum children. It can by retype for
	 * ITerminalExpression interface
	 * 
	 * @return true - if it is terminal symbol, otherwise false
	 */
	public boolean isTerminal() {
		return getMaxChilds() == 0 ? true : false;
	}

	/**
	 * Set name of expression
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Get name of expression
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * Get child expression of this expression
	 * 
	 * @return
	 */
	public List<Expression> getChild() {
		return child;
	}

	/**
	 * Maximum number of child for this expression
	 * 
	 * @return number of child expression
	 */
	public abstract int getMaxChilds();

	/**
	 * Get result for valuable expressions of each child expression
	 * 
	 * @return result of expression
	 */
	public abstract Double getResult();

	/**
	 * Is this expression empty? It is not still defined (set)
	 * 
	 * @return true - if it�s not set, otherwise false
	 */
	public boolean isNull() {
		if (this instanceof NullExpression)
			return true;
		return false;
	}

	/**
	 * Return parent expression
	 * 
	 * @return Parent expression of this expression
	 */
	public Expression getParent() {
		return parent;
	}

	/**
	 * Set parent expression
	 * 
	 * @param parent
	 *            parent expression
	 */
	public void setParent(Expression parent) {
		this.parent = parent;
	}

	/**
	 * Pick up all leaves (terminal expressions) from expression tree
	 * 
	 * @param leaves
	 *            all leaves, which are actually picked up
	 */
	public void takeLeaves(List<Expression> leaves) {
		// for each of child expression
		for (Expression expr : child) {
			// test for terminal expression
			if (!expr.isTerminal())
				// not terminal, then go to take leaves from this child
				expr.takeLeaves(leaves);
			else
				// else add this expression to list (as leave) and go to next
				// child
				leaves.add(expr);
		}
	}

	/**
	 * Pick up all expressions from tree to list
	 * 
	 * @param expressions
	 *            actually picked up expressions
	 */
	public void takeExpressions(List<Expression> expressions) {
		// for each of child expression
		for (Expression expr : child) {
			// add expression to list and take all expressions from child
			// expression
			expressions.add(expr);
			expr.takeExpressions(expressions);
		}
	}

	/**
	 * Pick up all null expression of leaves
	 * 
	 * @param expressions
	 *            actually picked up expressions
	 */
	public void takeNullExpressions(List<Expression> expressions) {
		for (Expression expr : child)
			expr.takeNullExpressions(expressions);
	}

	/**
	 * Depht of tree (from actual node)
	 * 
	 * @return
	 */
	public int getDepth() {
		int depth = 0;
		Expression expr = this;
		while (expr.getParent() != null) {
			depth++;
			expr = expr.getParent();
		}
		return depth;
	}

	/**
	 * Function arity. Number of arguments in function
	 * 
	 * @return Number of arguments in function
	 */
	public int getArita() {
		return arita;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (Expression expr : child)
			sb.append(expr.toString());
		sb.append(getName() + " ");
		return sb.toString();
	}

	/**
	 * Get instance of expression (copy)
	 * 
	 * @return
	 */
	public abstract Expression getInstance();

	public Expression createCopy() {
		return createCopy(null);
	}

	public abstract String prefixMode();

	private Expression createCopy(Expression rootNode) {
		Expression actualExpression = getInstance();
		actualExpression.setParent(rootNode);
		for (Expression childNode : getChild()) {
			actualExpression.add(childNode.createCopy(actualExpression));
		}
		return actualExpression;
	}
}
