package org.zigi.algorithm.evolution.gp.types;

public class TanFce extends Expression {

	public TanFce() {
		super(1);
		setName("tan()");
	}

	@Override
	public int getMaxChilds() {
		return 1;
	}

	@Override
	public Double getResult() {
		return Math.tan(child.get(0).getResult().doubleValue());
	}

	@Override
	public Expression getInstance() {
		return new TanFce();
	}

	@Override
	public String toString() {
		return "tan(" + getChild().get(0).toString() + ")";
	}

	@Override
	public String prefixMode() {
		// TODO Auto-generated method stub
		return null;
	}
}
