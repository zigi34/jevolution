package org.zigi.algorithm.evolution.gp;

import java.util.LinkedList;
import java.util.List;

import org.zigi.algorithm.evolution.EvolutionAlgorithm;
import org.zigi.algorithm.evolution.EvolutionAlgorithmState;
import org.zigi.algorithm.evolution.Population;
import org.zigi.algorithm.evolution.cross.CrossFunction;
import org.zigi.algorithm.evolution.cross.StructureOnePointCross;
import org.zigi.algorithm.evolution.genetic.GeneticAlgorithmState;
import org.zigi.algorithm.evolution.individual.ExpressionTree;
import org.zigi.algorithm.evolution.individual.Individual;
import org.zigi.algorithm.evolution.selection.RankSelection;
import org.zigi.algorithm.evolution.selection.SelectFunction;
import org.zigi.algorithm.problem.StructureProblem;

public class GeneticProgramming extends EvolutionAlgorithm<StructureProblem> {

	/* SELECTION */
	public static final double SELECT_MAX = 1.0;
	public static final double SELECT_MIN = 0.5;
	private double select = 0.6;

	/* MUTATION */
	public static final double MUTATE_MIN = 0.0;
	public static final double MUTATE_MAX = 0.8;
	private double mutate = 0.5;

	/* EVOLUTION OPERATORS */
	private SelectFunction selectFunction = new RankSelection();
	private CrossFunction crossFunction = new StructureOnePointCross();

	private List<Individual> unfeasibleSolutions = new LinkedList<Individual>();

	private int maxDepth = 1;
	private boolean complete = false;

	@Override
	public void run() {
		startRunning();
		setCurrGeneration(0);
		fireStateListener(GeneticAlgorithmState.STARTED_STATE);
		try {
			setBestSolution(getPopulation().get(0)); // actually best solution

			// Initial evaluation and check best individual
			Population.sortPopulation(getPopulation(), getProblem());
			refreshBest((ExpressionTree) getPopulation().get(0));

			List<Individual> list = new LinkedList<Individual>();

			while (getCurrGeneration() < getMaxGeneration() && isRunning()) {
				List<Individual> selected = getSelectFunction().select(
						getSelect(), getProblem(), getPopulation());
				fireStateListener(GeneticAlgorithmState.SELECTED_STATE);
				// mutace jedinc�
				for (Individual item : selected) {
					// Choose either mutate or cross
					if (random.nextDouble() < 0.5) {
						// mutate
						list.add(mutate(item, isComplete()));
					} else {
						// cross
						Individual ind1 = selected.get(random.nextInt(selected
								.size()));
						Individual ind2 = selected.get(random.nextInt(selected
								.size()));
						list.addAll(getCrossFunction().cross(ind1, ind2));
					}
				}

				selected.addAll(list);
				list.clear();

				// Vygenerov�n�
				int populationSize = getPopulation().size();
				List<Individual> pars = elitismus(selected, populationSize);
				getPopulation().clear();
				getPopulation().addAll(pars);
				fireStateListener(EvolutionAlgorithmState.NEW_POPULATION_CREATED_STATE);

				// Ohodnocen� populace
				Population.sortPopulation(getPopulation(), getProblem());
				refreshBest(getPopulation().get(0));

				incrementGeneration();
				unfeasibleSolutions.clear();
			}
		} catch (InterruptedException ie) {
			ie.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		stopRunning();
		fireStateListener(GeneticAlgorithmState.ENDED_STATE);
	}

	private List<Individual> selectFeasibleSolutions(
			final List<Individual> allSolutions) {
		List<Individual> feasibleSolutions = new LinkedList<Individual>();
		for (Individual solution : allSolutions) {
			Double fitness = getProblem().getFitnessValue(solution);
			if (fitness >= 0)
				feasibleSolutions.add(solution);
			else
				unfeasibleSolutions.add(solution);
		}
		return feasibleSolutions;
	}

	public double getSelect() {
		return select;
	}

	public void setSelect(double select) {
		this.select = select;
	}

	/**
	 * Return mutate probability (mutate = 1 - cross)
	 * 
	 * @return
	 */
	public double getMutate() {
		return mutate;
	}

	/**
	 * Set mutate probability (mutate = 1 - cross)
	 * 
	 * @param mutate
	 */
	public void setMutate(double mutate) {
		this.mutate = mutate;
	}

	/**
	 * Set cross probability (cross = 1 - mutate)
	 * 
	 * @param cross
	 *            cross probability
	 */
	public void setCross(double cross) {
		this.mutate = 1.0 - cross;
	}

	/**
	 * Return probability of cross operation (1 - cross = mutate)
	 * 
	 * @return cross probability
	 */
	public double getCross() {
		return 1.0 - mutate;
	}

	public int getMaxDepth() {
		return maxDepth;
	}

	public void setMaxDepth(int maxDepth) {
		this.maxDepth = maxDepth;
	}

	public boolean isComplete() {
		return complete;
	}

	public void setComplete(boolean complete) {
		this.complete = complete;
	}

	public SelectFunction getSelectFunction() {
		return selectFunction;
	}

	public void setSelectFunction(SelectFunction selectFunction) {
		this.selectFunction = selectFunction;
	}

	public CrossFunction getCrossFunction() {
		return crossFunction;
	}

	public void setCrossFunction(CrossFunction crossFunction) {
		this.crossFunction = crossFunction;
	}

	private List<Individual> elitismus(List<Individual> childrens,
			int populationSize) {
		List<Individual> result = new LinkedList<Individual>();
		Population.sortPopulation(childrens, getProblem());

		int i = 0;
		while (result.size() < populationSize) {
			result.add(childrens.get(i));
			i++;
		}
		return result;
	}

	private Individual mutate(Individual individual, boolean complete)
			throws Exception {
		ExpressionTree tree = (ExpressionTree) individual.createCopy();
		tree.mutate(getProblem().getTerminalList(), getProblem()
				.getNeterminalList(), getMaxDepth(), complete);
		return tree;
	}
}
