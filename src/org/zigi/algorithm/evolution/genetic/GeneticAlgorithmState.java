package org.zigi.algorithm.evolution.genetic;

import org.zigi.algorithm.evolution.EvolutionAlgorithmState;

public class GeneticAlgorithmState extends EvolutionAlgorithmState {

	private static final int CROSSED = 5;
	private static final int MUTATED = 6;
	private static final int SELECTED = 7;

	public static EvolutionAlgorithmState CROSSED_STATE = new GeneticAlgorithmState(
			CROSSED);
	public static EvolutionAlgorithmState MUTATED_STATE = new GeneticAlgorithmState(
			MUTATED);
	public static EvolutionAlgorithmState SELECTED_STATE = new GeneticAlgorithmState(
			SELECTED);

	private GeneticAlgorithmState() {
		super();
	}

	private GeneticAlgorithmState(int state) {
		super(state);
	}
}
