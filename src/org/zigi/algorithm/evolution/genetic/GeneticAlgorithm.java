package org.zigi.algorithm.evolution.genetic;

import java.util.LinkedList;
import java.util.List;

import org.zigi.algorithm.evolution.EvolutionAlgorithm;
import org.zigi.algorithm.evolution.EvolutionAlgorithmState;
import org.zigi.algorithm.evolution.Population;
import org.zigi.algorithm.evolution.cross.CrossFunction;
import org.zigi.algorithm.evolution.cross.NumericOnePointCross;
import org.zigi.algorithm.evolution.individual.Individual;
import org.zigi.algorithm.evolution.individual.Parameters;
import org.zigi.algorithm.evolution.selection.RouleteWheelSelection;
import org.zigi.algorithm.evolution.selection.SelectFunction;
import org.zigi.algorithm.problem.NumberProblem;

public class GeneticAlgorithm extends EvolutionAlgorithm<NumberProblem> {
	/* SELECTION */
	public static final double maxSelection = 1.0;
	public static final double minSelection = 0.5;
	public double selectProbability = 0.6;

	/* MUTATION */
	public static final double minMutate = 0.0;
	public static final double maxMutate = 0.8;
	private double mutateRange = 0.5;

	/* EVOLUTION OPERATORS */
	private SelectFunction selectFce = new RouleteWheelSelection();
	private CrossFunction crossFce = new NumericOnePointCross();

	/**
	 * Konstruktor
	 */
	public GeneticAlgorithm() {
		super();
	}

	@Override
	public String toString() {
		return "Genetick� algoritmus";
	}

	public void run() {
		startRunning();
		setCurrGeneration(0);
		fireStateListener(GeneticAlgorithmState.STARTED_STATE);
		try {
			setBestSolution(getPopulation().get(0)); // actually best solution

			// Initial evaluation and check best individual
			Population.sortPopulation(getPopulation(), getProblem());
			refreshBest((Parameters) getPopulation().get(0));

			while (getCurrGeneration() < getMaxGeneration() && isRunning()) {
				List<Individual> selected = getSelectFce().select(
						getSelectProbability(), getProblem(), getPopulation());

				fireStateListener(GeneticAlgorithmState.SELECTED_STATE);

				// Křížení vybraných jedinců pro generování potomků
				List<Parameters> newPopulation = new LinkedList<Parameters>();
				getCrossFce().cross(selected, newPopulation);

				fireStateListener(GeneticAlgorithmState.CROSSED_STATE);

				// mutace jedinců nové populace
				mutate(newPopulation, getMutateRange());

				fireStateListener(GeneticAlgorithmState.MUTATED_STATE);

				// Vygenerování nové populace pro novou generaci
				int populationSize = getPopulation().size();
				List<Parameters> pars = elitismus(newPopulation, populationSize);
				getPopulation().clear();
				getPopulation().addAll(pars);
				fireStateListener(EvolutionAlgorithmState.NEW_POPULATION_CREATED_STATE);

				// Ohodnocení populace a výběr nového nejlepšího jedince
				Population.sortPopulation(getPopulation(), getProblem());
				refreshBest((Parameters) getPopulation().get(0));

				incrementGeneration();

				Thread.sleep(1);
			}
		} catch (InterruptedException ie) {
			ie.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		stopRunning();
		fireStateListener(GeneticAlgorithmState.ENDED_STATE);
	}

	private List<Parameters> elitismus(List<Parameters> childrens,
			int populationSize) {
		List<Parameters> result = new LinkedList<Parameters>();
		result.addAll(childrens);

		int i = 0;
		while (result.size() < populationSize) {
			result.add((Parameters) getPopulation().get(i));
			i++;
		}
		return result;
	}

	private void mutate(List<Parameters> solutions, double mutateRange) {
		long size = Math.round(solutions.size() * mutateRange);
		for (int i = 0; i < size; i++) {
			int randPos = random.nextInt(getProblem().getRanges().size());
			solutions.get(i).set(randPos, getProblem().getRandomValue(randPos));
		}
	}

	/* SELECT PROBABILITY */
	public double getSelectProbability() {
		return selectProbability;
	}

	public void setSelectProbability(double probability) {
		this.selectProbability = probability;
		if (probability > maxSelection)
			this.selectProbability = maxSelection;
		if (probability < minSelection)
			this.selectProbability = minSelection;
	}

	/* SELECT FUNCTION */
	public SelectFunction getSelectFce() {
		return selectFce;
	}

	public void setSelectFce(SelectFunction selectFce) {
		this.selectFce = selectFce;
	}

	/* RANGE OF MUTATE */
	public double getMutateRange() {
		return mutateRange;
	}

	public void setMutateRange(double mutateRange) {
		this.mutateRange = mutateRange;
		if (mutateRange > maxMutate)
			this.mutateRange = maxMutate;
		if (mutateRange < minMutate)
			this.mutateRange = minMutate;
	}

	/* CROSS FUNCTIONS */
	public CrossFunction getCrossFce() {
		return crossFce;
	}

	public void setCrossFce(CrossFunction crossFce) {
		this.crossFce = crossFce;
	}
}
