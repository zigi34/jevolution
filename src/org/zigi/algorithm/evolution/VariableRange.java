package org.zigi.algorithm.evolution;

import java.util.Random;

public class VariableRange<T extends Number> {
	private T minValue;
	private T maxValue;

	private static Random random = new Random();

	public VariableRange(T min, T max) {
		this.minValue = min;
		this.maxValue = max;
	}

	public T getMinValue() {
		return minValue;
	}

	public void setMinValue(T minValue) {
		this.minValue = minValue;
	}

	public T getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(T maxValue) {
		this.maxValue = maxValue;
	}

	public VariableRange scaleRange(double percent) {
		T center = getCenterValue();
		T diff = getDifference();
		if (center instanceof Byte || center instanceof Short
				|| center instanceof Integer || center instanceof Long) {
			return new VariableRange<T>((T) new Long(center.longValue()
					- diff.longValue() / 2l * new Double(percent).longValue()),
					(T) new Long(center.longValue() + diff.longValue() / 2l
							* new Double(percent).longValue()));
		} else if (center instanceof Float || center instanceof Double) {
			return new VariableRange<T>((T) new Double(center.doubleValue()
					- diff.doubleValue() / 2.0 * percent), (T) new Double(
					center.doubleValue() + diff.doubleValue() / 2.0 * percent));
		}
		return null;
	}

	public void setRange(T range) {
		T min = getMinValue();
		T max = getMaxValue();
		if (min instanceof Byte || min instanceof Short
				|| min instanceof Integer || min instanceof Long) {
			setMaxValue((T) new Long(getCenterValue().longValue()
					+ new Long(range.longValue() / 2l)));
			setMinValue((T) new Long((Long) getCenterValue()
					- new Long(range.longValue() / 2l)));
		} else if (min instanceof Float || min instanceof Double) {
			setMaxValue((T) new Double(getCenterValue().doubleValue()
					+ new Double(range.doubleValue() / 2.0)));
			setMinValue((T) new Double(getCenterValue().doubleValue()
					- new Double(range.doubleValue() / 2.0)));
		}
	}

	public T getDifference() {
		T min = getMinValue();
		T max = getMaxValue();
		if (min instanceof Byte || min instanceof Short
				|| min instanceof Integer || min instanceof Long) {
			return (T) new Long(max.longValue() - min.longValue());
		} else if (min instanceof Float || min instanceof Double) {
			return (T) new Double(max.doubleValue() - min.doubleValue());
		}
		return null;
	}

	private T getCenterValue() {
		T min = getMinValue();
		T max = getMaxValue();
		if (min instanceof Byte || min instanceof Short
				|| min instanceof Integer || min instanceof Long) {
			return (T) new Long((max.longValue() - min.longValue()) / 2l
					+ min.longValue());
		} else if (min instanceof Float || min instanceof Double) {
			return (T) new Double((max.doubleValue() - min.doubleValue()) / 2.0
					+ min.doubleValue());
		}
		return null;
	}

	public void setCenterValue(T center) {
		T cen = getCenterValue();
		T min = getMinValue();
		T max = getMaxValue();
		if (cen instanceof Byte || cen instanceof Integer
				|| cen instanceof Short || cen instanceof Long) {
			Long cnt = cen.longValue();
			Long m = min.longValue();
			Long x = max.longValue();
			Long df = center.longValue() - cnt;

			this.minValue = (T) new Long(m + df);
			this.maxValue = (T) new Long(x + df);
		} else if (cen instanceof Float || cen instanceof Double) {
			Double cnt = cen.doubleValue();
			Double m = min.doubleValue();
			Double x = max.doubleValue();
			Double df = new Double(center.doubleValue() - cnt);

			this.minValue = (T) new Double(m + df);
			this.maxValue = (T) new Double(x + df);
		}
	}

	public T getRandomNumber() {
		T diff = getDifference();
		double rnd = random.nextDouble();
		if (diff instanceof Byte || diff instanceof Integer
				|| diff instanceof Short || diff instanceof Long) {
			Double min = getMinValue().doubleValue();
			return (T) new Long(
					new Double(rnd * diff.doubleValue() + min).longValue());
		} else if (diff instanceof Float || diff instanceof Double) {
			Double min = getMinValue().doubleValue();
			return (T) new Double(rnd * diff.doubleValue() + min);
		}
		return null;
	}
}
