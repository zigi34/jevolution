package org.zigi.algorithm.evolution.problems;

import org.zigi.algorithm.evolution.individual.Individual;
import org.zigi.algorithm.evolution.individual.Parameters;
import org.zigi.algorithm.problem.NumberProblem;

public class RanaFunction extends NumberProblem {
	public RanaFunction(String name) {
		super(name);
		fMin = -500;
		fMax = 500;
	}

	@Override
	public Individual getRandomIndividual() {
		return getRandomParameters();
	}

	@Override
	public double getSolutionValue(Parameters pars) {
		double sum = 0.0;
		for (int i = 0; i < pars.size() - 1; i++) {
			sum += pars.get(i).doubleValue()
					* Math.sin(Math.sqrt(Math.abs(pars.get(i + 1).doubleValue()
							+ 1 - pars.get(i).doubleValue())))
					* Math.cos(Math.sqrt(Math.abs(pars.get(i + 1).doubleValue()
							+ 1 + pars.get(i).doubleValue())))
					+ (pars.get(i + 1).doubleValue() + 1)
					* Math.cos(Math.sqrt(Math.abs(pars.get(i + 1).doubleValue()
							+ 1 - pars.get(i).doubleValue())))
					* Math.sin(Math.sqrt(Math.abs(pars.get(i + 1).doubleValue()
							+ 1 + pars.get(i).doubleValue())));
		}
		return sum;
	}
}
