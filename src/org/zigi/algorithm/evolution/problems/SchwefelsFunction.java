package org.zigi.algorithm.evolution.problems;

import org.zigi.algorithm.evolution.individual.Individual;
import org.zigi.algorithm.evolution.individual.Parameters;
import org.zigi.algorithm.problem.NumberProblem;

public class SchwefelsFunction extends NumberProblem {

	public SchwefelsFunction(String name) {
		super(name);
		fMin = -850;
		fMax = 850;
	}

	@Override
	public Individual getRandomIndividual() {
		return getRandomParameters();
	}

	@Override
	public double getSolutionValue(Parameters individual) {
		double sum = 0.0;
		for (int i = 0; i < individual.size(); i++) {
			sum += -individual.get(i).doubleValue()
					* Math.sin(Math.sqrt(Math.abs(individual.get(i)
							.doubleValue())));
		}
		return sum;
	}
}
