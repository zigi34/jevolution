package org.zigi.algorithm.evolution.problems;

import org.zigi.algorithm.evolution.individual.Individual;
import org.zigi.algorithm.evolution.individual.Parameters;
import org.zigi.algorithm.problem.NumberProblem;

public class PathologicFunction extends NumberProblem {
	public PathologicFunction(String name) {
		super(name);
		fMin = 0;
		fMax = 1;
	}

	@Override
	public Individual getRandomIndividual() {
		return getRandomParameters();
	}

	@Override
	public double getSolutionValue(Parameters pars) {
		double sum = 0.0;
		for (int i = 0; i < pars.size() - 1; i++) {
			sum += 0.5 + ((Math.pow(
					Math.sin(Math.sqrt(100
							* Math.pow(pars.get(i).doubleValue(), 2)
							- Math.pow(pars.get(i + 1).doubleValue(), 2))), 2) - 0.5) / (1 + 0.001 * Math
					.pow(Math.pow(pars.get(i).doubleValue(), 2) - 2
							* pars.get(i).doubleValue()
							* pars.get(i + 1).doubleValue()
							+ Math.pow(pars.get(i + 1).doubleValue(), 2), 2)));
		}
		return sum;
	}
}
