package org.zigi.algorithm.evolution.problems;

import org.zigi.algorithm.evolution.individual.Individual;
import org.zigi.algorithm.evolution.individual.Parameters;
import org.zigi.algorithm.problem.NumberProblem;

public class AxisParallelHyperbolicFunction extends NumberProblem {

	public AxisParallelHyperbolicFunction(String name) {
		super(name);
	}

	@Override
	public Individual getRandomIndividual() {
		return getRandomParameters();
	}

	@Override
	public double getSolutionValue(Parameters pars) {
		double sum = 0.0;
		for (int i = 0; i < pars.size(); i++) {
			sum += (i + 1) * Math.pow((double) pars.get(i), 2);
		}
		return sum;
	}
}
