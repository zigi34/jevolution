package org.zigi.algorithm.evolution.problems;

import org.zigi.algorithm.evolution.individual.Individual;
import org.zigi.algorithm.evolution.individual.Parameters;
import org.zigi.algorithm.problem.NumberProblem;

public class GriewangFunction extends NumberProblem {

	public GriewangFunction(String name) {
		super(name);
		fMin = 0;
		fMax = 3;
	}

	@Override
	public Individual getRandomIndividual() {
		return getRandomParameters();
	}

	@Override
	public double getSolutionValue(Parameters pars) {
		double sum = 0.0;
		double nas = Math.cos(pars.get(0).doubleValue() / Math.sqrt(1));
		for (int i = 0; i < pars.size(); i++) {
			sum += Math.pow(pars.get(i).doubleValue(), 2) / 4000.0;
		}
		for (int i = 1; i < pars.size(); i++) {
			nas *= Math.cos(pars.get(i).doubleValue() / Math.sqrt(i + 1));
		}
		return 1 + sum - nas;
	}
}
