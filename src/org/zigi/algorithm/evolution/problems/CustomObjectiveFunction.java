package org.zigi.algorithm.evolution.problems;

import org.zigi.algorithm.evolution.individual.Individual;
import org.zigi.algorithm.evolution.individual.Parameters;
import org.zigi.algorithm.problem.NumberProblem;
import org.zigi.math.expression.ExpressBuilder;
import org.zigi.math.expression.ExpressionContainer;

public class CustomObjectiveFunction extends NumberProblem {

	private ExpressionContainer container;

	public CustomObjectiveFunction(String name, String expression) {
		super(name);
		container = ExpressBuilder.createExpression(expression);
	}

	public void setExpression(String expression) {
		container = ExpressBuilder.createExpression(expression);
	}

	public void setExpression(ExpressionContainer container) {
		this.container = container;
	}

	@Override
	public Individual getRandomIndividual() {
		return getRandomParameters();
	}

	@Override
	public double getSolutionValue(Parameters individual) {
		for (int i = 0; i < individual.size()
				&& i < container.getVariables().length; i++) {
			container.getExpression().setVariable(container.getVariables()[i],
					individual.get(i).doubleValue());
		}
		return container.getExpression().evaluate();
	}
}
