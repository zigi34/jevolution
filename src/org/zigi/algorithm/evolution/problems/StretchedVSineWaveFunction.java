package org.zigi.algorithm.evolution.problems;

import org.zigi.algorithm.evolution.individual.Individual;
import org.zigi.algorithm.evolution.individual.Parameters;
import org.zigi.algorithm.problem.NumberProblem;

public class StretchedVSineWaveFunction extends NumberProblem {

	public StretchedVSineWaveFunction(String name) {
		super(name);
		fMin = 0;
		fMax = 7;
	}

	@Override
	public Individual getRandomIndividual() {
		return getRandomParameters();
	}

	@Override
	public double getSolutionValue(Parameters individual) {
		double sum = 0.0;
		for (int i = 0; i < individual.size() - 1; i++) {
			sum += Math.pow(Math.sin(50 * Math.pow(
					Math.pow(individual.get(i).doubleValue(), 2)
							+ Math.pow(individual.get(i + 1).doubleValue(), 2),
					1.0 / 10.0)), 2)
					* Math.pow(Math.pow(individual.get(i).doubleValue(), 2)
							+ Math.pow(individual.get(i + 1).doubleValue(), 2),
							1.0 / 4.0) + 1;
		}
		return sum;
	}
}