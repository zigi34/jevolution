package org.zigi.algorithm.evolution.problems;

import org.zigi.algorithm.evolution.individual.Individual;
import org.zigi.algorithm.evolution.individual.Parameters;
import org.zigi.algorithm.problem.NumberProblem;

public class EggHolderFunction extends NumberProblem {
	public EggHolderFunction(String name) {
		super(name);
		fMin = -1000;
		fMax = 1000;
	}

	@Override
	public Individual getRandomIndividual() {
		return getRandomParameters();
	}

	@Override
	public double getSolutionValue(Parameters pars) {
		double sum = 0.0;
		for (int i = 0; i < pars.size() - 1; i++) {
			sum += -pars.get(i).doubleValue()
					* Math.sin(Math.sqrt(Math.abs(pars.get(i).doubleValue()
							- pars.get(i + 1).doubleValue() - 47)))
					- (pars.get(i + 1).doubleValue() + 47)
					* Math.sin(Math.sqrt(Math.abs(pars.get(i + 1).doubleValue()
							+ 47 + pars.get(i).doubleValue() / 2.0)));
		}
		return sum;
	}
}
