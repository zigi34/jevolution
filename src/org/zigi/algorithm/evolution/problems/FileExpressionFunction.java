package org.zigi.algorithm.evolution.problems;

import java.io.File;
import java.io.IOException;

import org.zigi.algorithm.evolution.individual.Individual;
import org.zigi.algorithm.evolution.individual.Parameters;
import org.zigi.algorithm.evolution.util.ExpressionHelper;
import org.zigi.algorithm.problem.NumberProblem;
import org.zigi.math.expression.ExpressBuilder;
import org.zigi.math.expression.ExpressionContainer;

public class FileExpressionFunction extends NumberProblem {

	private File file;
	private ExpressionContainer expression;

	public FileExpressionFunction(String name, File file) {
		super(name);
		setFile(file);
		createExpression(getFile());
	}

	@Override
	public Individual getRandomIndividual() {
		return getRandomParameters();
	}

	private void createExpression(File file) {
		if (file == null)
			throw new NullPointerException();

		try {
			setExpressionContainer(ExpressBuilder.createExpression(getFile()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public ExpressionContainer getExpressionContainer() {
		return expression;
	}

	public void setExpressionContainer(ExpressionContainer expression) {
		this.expression = expression;
	}

	@Override
	public double getSolutionValue(Parameters pars) {
		return ExpressionHelper.evaluate(getExpressionContainer(), pars);
	}
}
