package org.zigi.algorithm.evolution.problems;

import org.zigi.algorithm.evolution.individual.Individual;
import org.zigi.algorithm.evolution.individual.Parameters;
import org.zigi.algorithm.problem.NumberProblem;

public class FourthDeJongFunction extends NumberProblem {
	public FourthDeJongFunction(String name) {
		super(name);
		fMin = 0;
		fMax = 3;
	}

	@Override
	public Individual getRandomIndividual() {
		return getRandomParameters();
	}

	@Override
	public double getSolutionValue(Parameters pars) {
		double sum = 0.0;
		for (int i = 0; i < pars.size(); i++) {
			sum += (i + 1) * Math.pow(pars.get(i).doubleValue(), 4);
		}
		return sum;
	}
}
