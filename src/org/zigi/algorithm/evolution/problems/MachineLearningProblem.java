package org.zigi.algorithm.evolution.problems;

import org.zigi.algorithm.evolution.gp.config.MeasuredConfiguration;
import org.zigi.algorithm.evolution.individual.ExpressionTree;
import org.zigi.algorithm.evolution.individual.Individual;
import org.zigi.algorithm.problem.StructureProblem;

public class MachineLearningProblem extends StructureProblem {
	private MeasuredConfiguration configuration = new MeasuredConfiguration();

	public MachineLearningProblem(String name) {
		super(name);
	}

	@Override
	public double getFitnessValue(Individual pars) {
		ExpressionTree solutionTree = (ExpressionTree) pars;
		Double deviation = configuration.getDeviation(solutionTree);
		if (deviation == 0.0)
			return Double.MAX_VALUE;
		return 1.0 / deviation;
	}

	public void setConfiguration(MeasuredConfiguration configuration) {
		this.configuration = configuration;
	}

	public MeasuredConfiguration getConfiguration() {
		return configuration;
	}
}
