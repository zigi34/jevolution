package org.zigi.algorithm.evolution.problems;

import org.zigi.algorithm.evolution.individual.Individual;
import org.zigi.algorithm.evolution.individual.Parameters;
import org.zigi.algorithm.problem.NumberProblem;

public class ThirdDeJongFunction extends NumberProblem {

	public ThirdDeJongFunction(String name) {
		super(name);
		fMin = 0;
		fMax = 4;
	}

	@Override
	public Individual getRandomIndividual() {
		return getRandomParameters();
	}

	@Override
	public double getSolutionValue(Parameters individual) {
		double sum = 0.0;
		for (int i = 0; i < individual.size(); i++) {
			sum += Math.abs(individual.get(i).doubleValue());
		}
		return sum;
	}
}
