package org.zigi.algorithm.evolution.problems;

import org.zigi.algorithm.evolution.individual.Individual;
import org.zigi.algorithm.evolution.individual.Parameters;
import org.zigi.algorithm.problem.NumberProblem;

public class FirstDeJongFunction extends NumberProblem {

	public FirstDeJongFunction(String name) {
		super(name);
		fMin = 0;
		fMax = 50;
	}

	@Override
	public Individual getRandomIndividual() {
		return getRandomParameters();
	}

	@Override
	public double getSolutionValue(Parameters individual) {
		double sum = 0.0;
		for (int i = 0; i < individual.size(); i++) {
			sum += Math.abs(Math.pow(individual.get(i).doubleValue(),
					individual.size()));
		}
		return sum;
	}
}
