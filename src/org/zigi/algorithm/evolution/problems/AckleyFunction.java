package org.zigi.algorithm.evolution.problems;

import org.zigi.algorithm.evolution.individual.Individual;
import org.zigi.algorithm.evolution.individual.Parameters;
import org.zigi.algorithm.problem.NumberProblem;

public class AckleyFunction extends NumberProblem {
	public AckleyFunction(String name) {
		super(name);
		fMin = -5;
		fMax = 10;
	}

	@Override
	public double getSolutionValue(Parameters pars) {
		double sum = 0.0;
		for (int i = 0; i < pars.size() - 1; i++) {
			sum += Math.sqrt(Math.pow(pars.get(i).doubleValue(), 2)
					+ Math.pow(pars.get(i + 1).doubleValue(), 2))
					/ Math.pow(Math.E, 5)
					+ 3
					* (Math.cos(2 * pars.get(i).doubleValue()) + Math
							.sin(2 * pars.get(i + 1).doubleValue()));
		}
		return sum;
	}

	@Override
	public Individual getRandomIndividual() {
		return getRandomParameters();
	}
}
