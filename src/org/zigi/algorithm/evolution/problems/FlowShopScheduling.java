package org.zigi.algorithm.evolution.problems;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.zigi.algorithm.evolution.individual.Individual;
import org.zigi.algorithm.evolution.individual.Parameters;
import org.zigi.algorithm.problem.PermutativeProblem;

public class FlowShopScheduling extends PermutativeProblem {
	private int machineCount = 0;
	private int[][] times;
	private boolean initialized = false;

	public FlowShopScheduling(String name) {
		super(name);
	}

	@Override
	public Individual getRandomIndividual() {
		return getRandomPermutation();
	}

	@Override
	public int getTimeSpan(Parameters pars) {
		List<Integer> permutation = new ArrayList<>(pars.size());
		for (int i = 0; i < pars.size(); i++)
			permutation.add(pars.get(i).intValue());

		int[][] timespan = new int[getMachineCount()][getJobCount()];

		// creat times for first machine
		for (int i = 0; i < permutation.size(); i++) {
			timespan[0][i] = ((i > 0) ? timespan[0][i - 1] : 0)
					+ getTimes()[0][permutation.get(i)];
		}

		// creat times for first job
		for (int i = 0; i < getMachineCount(); i++) {
			timespan[i][0] = ((i > 0) ? timespan[i - 1][0] : 0)
					+ getTimes()[i][permutation.get(0)];
		}

		// set all timespan
		for (int m = 1; m < getMachineCount(); m++)
			for (int j = 1; j < getJobCount(); j++) {
				timespan[m][j] = max(timespan[m - 1][j], timespan[m][j - 1])
						+ getTimes()[m][permutation.get(j)];
			}

		return timespan[getMachineCount() - 1][getJobCount() - 1];
	}

	public int getMachineCount() {
		return machineCount;
	}

	public void setMachineCount(int machineCount) {
		this.machineCount = machineCount;
	}

	public int getJobCount() {
		return getDimension();
	}

	public void setJobCount(int jobCount) {
		setDimension(jobCount);
	}

	private boolean initializeProblem() throws Exception {
		if (getMachineCount() == 0 || getJobCount() == 0)
			throw new Exception("Machine count or Job count is not set");
		times = new int[machineCount][getDimension()];
		return true;
	}

	public void addValue(int machine, int job, int time) throws Exception {
		if (!initialized)
			initialized = initializeProblem();
		times[machine][job] = time;
	}

	public void loadFromFile(File file) throws Exception {
		if (file.exists()) {
			BufferedReader reader = new BufferedReader(new FileReader(file));
			List<String> lines = new LinkedList<>();
			while (true) {
				String line = reader.readLine();
				if (line == null)
					break;
				lines.add(line);
			}
			reader.close();

			if (lines.size() > 0) {
				int jobs = lines.get(0).split(" ").length;
				int machines = lines.size();
				setLo(0);
				setHi(jobs - 1);

				setMachineCount(machines);
				setJobCount(jobs);

				for (int m = 0; m < machines; m++) {
					String[] vals = lines.get(m).split(" ");
					for (int j = 0; j < vals.length; j++) {
						addValue(m, j, Integer.parseInt(vals[j]));
					}
				}
			}
		}
	}

	public int[][] getTimes() {
		return times;
	}

	@Override
	public double getSolutionValue(Individual pars) {
		return getTimeSpan((Parameters) pars);
	}
}
