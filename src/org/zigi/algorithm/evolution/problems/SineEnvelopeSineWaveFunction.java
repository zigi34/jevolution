package org.zigi.algorithm.evolution.problems;

import org.zigi.algorithm.evolution.individual.Individual;
import org.zigi.algorithm.evolution.individual.Parameters;
import org.zigi.algorithm.problem.NumberProblem;

public class SineEnvelopeSineWaveFunction extends NumberProblem {

	public SineEnvelopeSineWaveFunction(String name) {
		super(name);
		fMax = -0.5;
		fMin = -1.5;
	}

	@Override
	public Individual getRandomIndividual() {
		return getRandomParameters();
	}

	@Override
	public double getSolutionValue(Parameters individual) {
		double sum = 0.0;
		for (int i = 0; i < individual.size() - 1; i++) {
			sum += 0.5 + (Math.sin(Math.pow(
					Math.pow(individual.get(i).doubleValue(), 2)
							+ Math.pow(individual.get(i + 1).doubleValue(), 2)
							- 0.5, 2)) / Math
					.pow(1 + 0.001 * (Math.pow(individual.get(i).doubleValue(),
							2) + Math.pow(individual.get(i + 1).doubleValue(),
							2)), 2));
		}
		return -sum;
	}
}
