package org.zigi.algorithm.evolution.problems;

import org.zigi.algorithm.evolution.individual.Individual;
import org.zigi.algorithm.evolution.individual.Parameters;
import org.zigi.algorithm.problem.NumberProblem;

public class AckleyFunction2 extends NumberProblem {

	public AckleyFunction2(String name) {
		super(name);
		fMin = 0;
		fMax = 20;
	}

	@Override
	public double getSolutionValue(Parameters pars) {
		double sum = 0.0;
		for (int i = 0; i < pars.size() - 1; i++) {
			sum += 20
					+ Math.E
					- (20.0 / Math.pow(Math.E, Math.pow((Math.pow(pars.get(i)
							.doubleValue(), 2) + Math.pow(pars.get(i + 1)
							.doubleValue(), 2)) / 2.0, 0.2)))
					- Math.pow(
							Math.E,
							0.5 * (Math.cos(2
									* Math.PI
									* pars.get(i).doubleValue()
									+ Math.cos(2 * Math.PI
											* pars.get(i + 1).doubleValue()))));
		}
		return sum;
	}

	@Override
	public Individual getRandomIndividual() {
		return getRandomParameters();
	}
}
