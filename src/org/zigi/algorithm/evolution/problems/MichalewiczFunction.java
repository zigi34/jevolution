package org.zigi.algorithm.evolution.problems;

import org.zigi.algorithm.evolution.individual.Individual;
import org.zigi.algorithm.evolution.individual.Parameters;
import org.zigi.algorithm.problem.NumberProblem;

public class MichalewiczFunction extends NumberProblem {

	public MichalewiczFunction(String name) {
		super(name);
		fMin = -2;
		fMax = 0;
	}

	@Override
	public Individual getRandomIndividual() {
		return getRandomParameters();
	}

	@Override
	public double getSolutionValue(Parameters individual) {
		double sum = 0.0;
		for (int i = 0; i < individual.size() - 1; i++) {
			sum += -1
					* (Math.sin(individual.get(i).doubleValue())
							* Math.pow(Math.sin(Math.pow(individual.get(i)
									.doubleValue(), 2)
									/ Math.PI), 20.0) + Math.sin(individual
							.get(i + 1).doubleValue())
							* Math.pow(Math.sin(2
									* Math.pow(individual.get(i).doubleValue(),
											2) / Math.PI), 20));
		}
		return sum;
	}
}
