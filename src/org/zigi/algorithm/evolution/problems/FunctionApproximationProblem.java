package org.zigi.algorithm.evolution.problems;

import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.zigi.algorithm.evolution.VariableRange;
import org.zigi.algorithm.evolution.gp.types.Expression;
import org.zigi.algorithm.evolution.gp.types.VarExpr;
import org.zigi.algorithm.evolution.individual.ExpressionTree;
import org.zigi.algorithm.evolution.individual.Individual;
import org.zigi.algorithm.problem.StructureProblem;

public class FunctionApproximationProblem extends StructureProblem {

	private HashMap<String, VariableRange<Double>> ranges;
	private ExpressionTree function = null;

	public FunctionApproximationProblem(String name) {
		super(name);
	}

	public HashMap<String, VariableRange<Double>> getRanges() {
		return ranges;
	}

	public void setRanges(HashMap<String, VariableRange<Double>> ranges) {
		this.ranges = ranges;
	}

	public ExpressionTree getFunction() {
		return function;
	}

	public void setFunction(ExpressionTree function) {
		this.function = function;
	}

	@Override
	public double getFitnessValue(Individual pars) {
		ExpressionTree exprTree = (ExpressionTree) pars;
		ExpressionTree solution = getFunction();

		HashMap<String, VarExpr> parameters = new HashMap<>();
		List<Expression> treeExpressions = exprTree.getExpressions();
		for (Expression expr : treeExpressions) {
			if (expr.isTerminal() && (expr instanceof VarExpr)
					&& !parameters.containsKey(expr.getName()))
				parameters.put(expr.getName(), (VarExpr) expr);
		}

		// find all variables for solution tree
		for (Expression expr : solution.getExpressions()) {
			if (expr.isTerminal() && (expr instanceof VarExpr)
					&& !parameters.containsKey(expr.getName()))
				parameters.put(expr.getName(), (VarExpr) expr);
		}

		Random random = new Random();
		double sum = 0.0;
		for (int i = 0; i < 10; i++) {
			// set for variable any values
			for (VarExpr expr : parameters.values()) {
				double randVal = random.nextDouble()
						* (expr.getMaxValue() - expr.getMinValue())
						+ expr.getMinValue();
				expr.setValue(randVal);
			}

			// set value for each variables in individual tree
			for (Expression expr : treeExpressions) {
				if (expr.isTerminal() && (expr instanceof VarExpr)) {
					VarExpr varExpr = (VarExpr) expr;
					VarExpr item = parameters.get(varExpr.getName());
					varExpr.setValue(item.getValue());
				}
			}

			// set value for each variables in solution tree
			for (Expression expr : solution.getExpressions()) {
				if (expr.isTerminal() && (expr instanceof VarExpr)) {
					VarExpr varExpr = (VarExpr) expr;
					VarExpr item = parameters.get(varExpr.getName());
					varExpr.setValue(item.getValue());
				}
			}

			double solutionVal = solution.getResult();
			double individualVal = exprTree.getResult();

			double difference = Math.pow(solutionVal - individualVal, 2);
			sum += difference;
		}
		return sum;
	}
}
