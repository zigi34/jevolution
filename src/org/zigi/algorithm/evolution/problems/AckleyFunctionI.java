package org.zigi.algorithm.evolution.problems;

import org.zigi.algorithm.evolution.individual.Individual;
import org.zigi.algorithm.evolution.individual.Parameters;
import org.zigi.algorithm.problem.NumberProblem;

public class AckleyFunctionI extends NumberProblem {

	public AckleyFunctionI(String name) {
		super(name);
		fMin = -5;
		fMax = 30;
	}

	@Override
	public Individual getRandomIndividual() {
		return getRandomParameters();
	}

	@Override
	public double getSolutionValue(Parameters pars) {
		double sum1 = 0.0;
		double sum2 = 0.0;
		for (int i = 0; i < pars.size(); i++) {
			sum1 += Math.pow(pars.get(i).doubleValue(), 2);
		}
		for (int i = 0; i < pars.size(); i++) {
			sum2 += Math.cos(2 * Math.PI * pars.get(i).doubleValue());
		}
		return -20 * Math.exp(-0.2 * Math.sqrt(sum1 / pars.size()))
				- Math.exp(sum2 / pars.size()) + 20 + Math.exp(1);
	}
}
