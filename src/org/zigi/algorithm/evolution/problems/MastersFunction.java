package org.zigi.algorithm.evolution.problems;

import org.zigi.algorithm.evolution.individual.Individual;
import org.zigi.algorithm.evolution.individual.Parameters;
import org.zigi.algorithm.problem.NumberProblem;

public class MastersFunction extends NumberProblem {
	public MastersFunction(String name) {
		super(name);
		fMin = -1;
		fMax = 1;
	}

	@Override
	public Individual getRandomIndividual() {
		return getRandomParameters();
	}

	@Override
	public double getSolutionValue(Parameters pars) {
		double sum = 0.0;
		for (int i = 0; i < pars.size() - 1; i++) {
			sum += Math.pow(
					Math.E,
					-(Math.pow(pars.get(i).doubleValue(), 2)
							+ Math.pow(pars.get(i + 1).doubleValue(), 2) + 0.5
							* pars.get(i).doubleValue()
							* pars.get(i + 1).doubleValue()) / 8.0)
					* Math.cos(4 * Math.sqrt(Math.pow(
							pars.get(i).doubleValue(), 2)
							+ Math.pow(pars.get(i + 1).doubleValue(), 2)
							+ 0.5
							* pars.get(i).doubleValue()
							* pars.get(i + 1).doubleValue()));
		}
		return sum;
	}
}
