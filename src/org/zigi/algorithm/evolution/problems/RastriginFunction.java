package org.zigi.algorithm.evolution.problems;

import org.zigi.algorithm.evolution.individual.Individual;
import org.zigi.algorithm.evolution.individual.Parameters;
import org.zigi.algorithm.problem.NumberProblem;

public class RastriginFunction extends NumberProblem {
	public RastriginFunction(String name) {
		super(name);
		fMin = -400;
		fMax = 1000;
	}

	@Override
	public Individual getRandomIndividual() {
		return getRandomParameters();
	}

	@Override
	public double getSolutionValue(Parameters individual) {
		double sum = 0.0;
		for (int i = 0; i < individual.size(); i++) {
			sum += Math.pow(individual.get(i).doubleValue(), 2) - 10
					* Math.cos(2.0 * Math.PI * individual.get(i).doubleValue());
		}
		return 10 * individual.size() * sum;
	}
}
