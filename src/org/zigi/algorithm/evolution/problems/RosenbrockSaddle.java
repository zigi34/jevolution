package org.zigi.algorithm.evolution.problems;

import org.zigi.algorithm.evolution.individual.Individual;
import org.zigi.algorithm.evolution.individual.Parameters;
import org.zigi.algorithm.problem.NumberProblem;

public class RosenbrockSaddle extends NumberProblem {
	public RosenbrockSaddle(String name) {
		super(name);
		fMin = 0;
		fMax = 4000;
	}

	@Override
	public Individual getRandomIndividual() {
		return getRandomParameters();
	}

	@Override
	public double getSolutionValue(Parameters individual) {
		double sum = 0.0;
		for (int i = 0; i < individual.size() - 1; i++) {
			sum += 100
					* Math.pow((individual.get(i + 1).doubleValue() - Math.pow(
							individual.get(i).doubleValue(), 2)), 2)
					+ Math.pow((1 - individual.get(i).doubleValue()), 2);
		}
		return sum;
	}
}
