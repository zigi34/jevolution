package org.zigi.algorithm.evolution.es;

import org.zigi.algorithm.evolution.EvolutionAlgorithmState;

public class EvolutionStrategyState extends EvolutionAlgorithmState {

	private EvolutionStrategyState() {

	}

	private EvolutionStrategyState(int state) {
		super(state);
	}
}
