package org.zigi.algorithm.evolution.es;

import java.util.LinkedList;
import java.util.List;

import org.zigi.algorithm.evolution.EvolutionAlgorithm;
import org.zigi.algorithm.evolution.Population;
import org.zigi.algorithm.evolution.VariableRange;
import org.zigi.algorithm.evolution.de.DifferentialEvolutionState;
import org.zigi.algorithm.evolution.individual.Individual;
import org.zigi.algorithm.evolution.individual.Parameters;
import org.zigi.algorithm.problem.NumberProblem;
import org.zigi.algorithm.problem.Problem;

public class EvolutionStrategy extends EvolutionAlgorithm<NumberProblem>
		implements Runnable {

	public static final int POPULATION_MIN = 1;
	public static final int POPULATION_MAX = Integer.MAX_VALUE;
	public static final double REPRODUCTION_MIN = 4;
	public static final double REPRODUCTION_MAX = 1000;

	/* PARAMETERS */
	protected double reproductionRange = 2;
	protected int reproductionCount = 10;
	protected boolean withInherit = true;

	/**
	 * Konstruktor
	 */
	public EvolutionStrategy() {
	}

	@Override
	public String toString() {
		return "Evoluční strategie";
	}

	private void initialize() throws Exception {
		getProblem().initialize();
	}

	public void run() {
		startRunning();
		setCurrGeneration(0);
		try {
			fireStateListener(EvolutionStrategyState.STARTED_STATE);
			initialize();
			setBestSolution(getPopulation().get(0));
			while (isRunning() && getCurrGeneration() < getMaxGeneration()) {
				List<Individual> newPopulation = reproduction(getPopulation(),
						getProblem());
				List<Individual> selected = select(newPopulation);
				getPopulation().clear();
				getPopulation().addAll(selected);
				fireStateListener(EvolutionStrategyState.NEW_POPULATION_CREATED_STATE);
				refreshBest((Parameters) getPopulation().get(0));
				incrementGeneration();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		fireStateListener(EvolutionStrategyState.ENDED_STATE);
		stopRunning();
	}

	/**
	 * Aktualizuje globálního nejlepšího jedince a jeho fitness hodnotu
	 * 
	 * @param individual
	 */
	private void refreshBest(Parameters individual) {
		double fitnessVal = getProblem().getFitnessValue(individual);
		if (fitnessVal < getProblem().getFitnessValue(getBestSolution())) {
			setBestSolution(individual);
			fireStateListener(DifferentialEvolutionState.NEW_BEST_SOLUTION_STATE);
		}
	}

	/* REPRODUCTION */
	private List<Individual> reproduction(Population population,
			Problem function) throws Exception {
		List<Individual> result = new LinkedList<Individual>();
		for (int i = 0; i < population.size(); i++) {
			Parameters x = (Parameters) population.get(i);
			if (withInherit)
				result.add(x);
			List<VariableRange<Number>> ranges = new LinkedList<VariableRange<Number>>();
			for (int s = 0; s < x.size(); s++) {
				VariableRange<Number> r = new VariableRange<Number>(0.0,
						reproductionRange);
				r.setCenterValue(x.get(s).doubleValue());
				r.setRange(reproductionRange);
				ranges.add(r);
			}
			for (int j = 0; j < reproductionCount; j++) {
				Parameters xStripe = Parameters.getRandomParameters(ranges);
				result.add(xStripe);
			}
		}
		return result;
	}

	private List<Individual> select(List<Individual> pars) {
		List<Individual> result = new LinkedList<Individual>();
		int count = getPopulation().size();
		Population.sortPopulation(pars, getProblem());
		for (int i = 0; i < count; i++) {
			result.add(pars.get(i));
		}
		return result;
	}

	public void setReproductionRange(double value) {
		this.reproductionRange = value;
	}

	public void setReproductionCount(int value) {
		this.reproductionCount = value;
	}

	public void setStrategy(boolean selected) {
		this.withInherit = selected;
	}
}
