package org.zigi.algorithm.evolution;

public class EvolutionAlgorithmState {

	private static final int STARTED = 1;
	private static final int ENDED = 2;
	private static final int NEW_POPULATION_CREATED = 3;
	private static final int NEW_BEST_SOLUTION = 4;

	public static EvolutionAlgorithmState STARTED_STATE = new EvolutionAlgorithmState(
			STARTED);
	public static EvolutionAlgorithmState ENDED_STATE = new EvolutionAlgorithmState(
			ENDED);
	public static EvolutionAlgorithmState NEW_POPULATION_CREATED_STATE = new EvolutionAlgorithmState(
			NEW_POPULATION_CREATED);
	public static EvolutionAlgorithmState NEW_BEST_SOLUTION_STATE = new EvolutionAlgorithmState(
			NEW_BEST_SOLUTION);

	private int state;

	protected EvolutionAlgorithmState() {
	}

	protected EvolutionAlgorithmState(int state) {
		this.state = state;
	}
}
