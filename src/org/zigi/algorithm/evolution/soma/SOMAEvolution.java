package org.zigi.algorithm.evolution.soma;

import java.util.LinkedList;
import java.util.List;

import org.zigi.algorithm.evolution.EvolutionAlgorithm;
import org.zigi.algorithm.evolution.Population;
import org.zigi.algorithm.evolution.de.DifferentialEvolutionState;
import org.zigi.algorithm.evolution.individual.Parameters;
import org.zigi.algorithm.problem.NumberProblem;
import org.zigi.algorithm.problem.Problem;

public class SOMAEvolution extends EvolutionAlgorithm<NumberProblem> implements
		Runnable {

	public static final int POPULATION_MIN = 10;
	public static final int POPULATION_MAX = Integer.MAX_VALUE;
	public static final double STEP_MIN = 0.11;
	public static final double STEP_MAX = 2;
	public static final double PATH_MIN = 1.1;
	public static final double PATH_MAX = 5.0;
	public static final int GENERATION_MIN = 1;
	public static final int GENERATION_MAX = Integer.MAX_VALUE;

	/* PARAMETERS */
	protected double minDiv = 0.0;
	protected double prt = 0.3;
	protected double pathLength = 1.3;
	protected double step = 0.2;

	/**
	 * Konstruktor
	 */
	public SOMAEvolution() {
	}

	@Override
	public String toString() {
		return "SOMA";
	}

	private void initialize() throws Exception {
		getProblem().initialize();
	}

	public void run() {
		startRunning();
		setCurrGeneration(0);
		try {
			fireStateListener(SOMAEvolutionState.STARTED_STATE);
			initialize();
			setBestSolution(getPopulation().get(0));
			while (isRunning() && getCurrGeneration() < getMaxGeneration()) {
				Population.sortPopulation(getPopulation(), getProblem());
				refreshBest((Parameters) getPopulation().get(0));

				List<Parameters> newPopulation = reproduction(getPopulation(),
						(Parameters) getBestSolution(), getProblem());

				getPopulation().clear();
				getPopulation().addAll(newPopulation);

				fireStateListener(SOMAEvolutionState.NEW_POPULATION_CREATED_STATE);
				incrementGeneration();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		fireStateListener(SOMAEvolutionState.ENDED_STATE);
		stopRunning();
	}

	/**
	 * Aktualizuje globálního nejlepšího jedince a jeho fitness hodnotu
	 * 
	 * @param individual
	 */
	private void refreshBest(Parameters individual) {
		double fitnessVal = getProblem().getFitnessValue(individual);
		if (fitnessVal < getProblem().getFitnessValue(getBestSolution())) {
			setBestSolution(individual);
			fireStateListener(DifferentialEvolutionState.NEW_BEST_SOLUTION_STATE);
		}
	}

	/* REPRODUCTION */
	private List<Parameters> reproduction(Population population,
			Parameters leader, Problem function) throws Exception {
		List<Parameters> result = new LinkedList<Parameters>();

		for (int i = 0; i < population.size(); i++) {
			Parameters active = (Parameters) population.get(i);

			double bestVal = getProblem().getFitnessValue(active);
			Parameters bestPar = active;

			double length = step;
			while (length < pathLength) {
				Parameters mutated = Parameters.add(active,
						Parameters.difference(leader, active));
				for (int d = 0; d < active.size(); d++) {
					if (random.nextDouble() > prt)
						mutated.set(d, active.get(d));
				}

				if (getProblem().getFitnessValue(mutated) < bestVal) {
					bestVal = getProblem().getFitnessValue(mutated);
					bestPar = mutated;
				}

				length += step;
			}
			result.add(bestPar);
		}
		return result;
	}

	public void setPathLength(double value) {
		this.pathLength = value;
	}

	public void setStep(double value) {
		this.step = value;
	}

	public void setPRT(double value) {
		this.prt = value;
	}

	public void setMinDiv(double value) {
		this.minDiv = value;
	}
}
