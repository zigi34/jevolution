package org.zigi.algorithm.evolution.soma;

import org.zigi.algorithm.evolution.EvolutionAlgorithmState;

public class SOMAEvolutionState extends EvolutionAlgorithmState {

	private static final int STARTED = 1;
	private static final int ENDED = 2;
	private static final int NEW_BEST_SOLUTION = 3;

	public static EvolutionAlgorithmState STARTED_STATE = new SOMAEvolutionState(
			STARTED);
	public static EvolutionAlgorithmState ENDED_STATE = new SOMAEvolutionState(
			ENDED);
	public static EvolutionAlgorithmState NEW_BEST_SOLUTION_STATE = new SOMAEvolutionState(
			NEW_BEST_SOLUTION);

	public int state;

	private SOMAEvolutionState() {
	}

	private SOMAEvolutionState(int state) {
		this.state = state;
	}
}
