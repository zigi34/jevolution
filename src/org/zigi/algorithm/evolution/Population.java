package org.zigi.algorithm.evolution;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import org.zigi.algorithm.evolution.individual.Individual;
import org.zigi.algorithm.problem.NumberProblem;
import org.zigi.algorithm.problem.Problem;

public class Population implements Collection<Individual>, List<Individual> {

	private List<Individual> values = new LinkedList<Individual>();

	/**
	 * Evaluate and sort population from fitness evaluation individual
	 * 
	 * @param solutions
	 *            all individual of population
	 * @param function
	 *            objective function
	 */
	public static void sortPopulation(List<Individual> solutions,
			final Problem function) {
		// Se�azen� �e�en� podle fitness od nejlep��ho po nejhor��
		Collections.sort(solutions, new Comparator<Individual>() {
			public int compare(Individual o1, Individual o2) {
				double f1 = function.getFitnessValue(o1);
				double f2 = function.getFitnessValue(o2);
				if (f1 > f2)
					return -1; // proto�e �ad�me sestupn� a ne vzestupn�
								// (default)
				else if (f1 == f2)
					return 0;
				else
					return 1;
			};
		});
	}

	/**
	 * Evaluate and sort population from fitness evaluation individual
	 * 
	 * @param individuals
	 *            all individual of population
	 * @param function
	 *            objective function
	 */
	public static void sortPopulation(Population population,
			final Problem function) {
		sortPopulation(population.getParameters(), function);
	}

	public void initializePopulation(NumberProblem problem, int count) {
		values.clear();
		for (int i = 0; i < count; i++)
			values.add(problem.getRandomParameters());
	}

	public static Population createRandomPopulation(int count,
			NumberProblem problem) {
		Population pop = new Population();
		for (int i = 0; i < count; i++)
			pop.add(problem.getRandomParameters());
		return pop;
	}

	public List<Individual> getParameters() {
		return values;
	}

	@Override
	public int size() {
		return values.size();
	}

	@Override
	public boolean isEmpty() {
		return values.isEmpty();
	}

	@Override
	public boolean contains(Object o) {
		return values.contains(o);
	}

	@Override
	public Iterator<Individual> iterator() {
		return values.iterator();
	}

	@Override
	public Object[] toArray() {
		return values.toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		return values.toArray(a);
	}

	@Override
	public boolean add(Individual e) {
		return values.add(e);
	}

	@Override
	public boolean remove(Object o) {
		return values.remove(o);
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return values.containsAll(c);
	}

	@Override
	public boolean addAll(Collection<? extends Individual> c) {
		return values.addAll(c);
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		return values.removeAll(c);
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		return values.retainAll(c);
	}

	@Override
	public void clear() {
		values.clear();
	}

	@Override
	public boolean addAll(int index, Collection<? extends Individual> c) {
		return values.addAll(index, c);
	}

	@Override
	public Individual get(int index) {
		return values.get(index);
	}

	@Override
	public Individual set(int index, Individual element) {
		return values.set(index, element);
	}

	@Override
	public void add(int index, Individual element) {
		values.add(index, element);
	}

	@Override
	public Individual remove(int index) {
		return values.remove(index);
	}

	@Override
	public int indexOf(Object o) {
		return values.indexOf(o);
	}

	@Override
	public int lastIndexOf(Object o) {
		return values.lastIndexOf(o);
	}

	@Override
	public ListIterator<Individual> listIterator() {
		return values.listIterator();
	}

	@Override
	public ListIterator<Individual> listIterator(int index) {
		return values.listIterator(index);
	}

	@Override
	public List<Individual> subList(int fromIndex, int toIndex) {
		return values.subList(fromIndex, toIndex);
	}
}
