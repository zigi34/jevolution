package org.zigi.algorithm.evolution.selection;

import java.security.SecureRandom;
import java.util.List;

import org.zigi.algorithm.evolution.Population;
import org.zigi.algorithm.evolution.individual.Individual;
import org.zigi.algorithm.problem.Problem;

public abstract class SelectFunction {

	protected SecureRandom random = new SecureRandom();

	public abstract List<Individual> select(double probability,
			Problem problem, Population population);
}
