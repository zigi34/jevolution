package org.zigi.algorithm.evolution.selection;

import java.util.LinkedList;
import java.util.List;

import org.zigi.algorithm.evolution.Population;
import org.zigi.algorithm.evolution.individual.Individual;
import org.zigi.algorithm.problem.Problem;

public class StructuralRouleteSelection extends SelectFunction {

	@Override
	public List<Individual> select(double probability, Problem problem,
			Population population) {
		List<Double> fitnesses = new LinkedList<Double>();
		List<Individual> acceptedSolutions = new LinkedList<Individual>();
		double sumValue = 0.0;
		for (Individual item : population) {
			double value = problem.getFitnessValue(item);
			if (Double.isInfinite(value))
				continue;
			acceptedSolutions.add(item);
			sumValue += value;
			fitnesses.add(sumValue);
		}

		List<Individual> result = new LinkedList<>();
		int maxItems = (int) (acceptedSolutions.size() * probability);
		if (maxItems % 2 == 1)
			maxItems -= 1;
		for (int index = 0; index < maxItems; index++) {
			double rand = random.nextDouble() * sumValue;
			int position = 0;
			while (position < fitnesses.size()
					&& fitnesses.get(position) < rand) {
				position++;
			}
			if (position == fitnesses.size())
				position -= 1;
			result.add(acceptedSolutions.get(position));
		}
		return result;
	}

}
