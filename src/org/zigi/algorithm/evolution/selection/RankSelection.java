package org.zigi.algorithm.evolution.selection;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import org.zigi.algorithm.evolution.Population;
import org.zigi.algorithm.evolution.individual.Individual;
import org.zigi.algorithm.problem.Problem;

public class RankSelection extends SelectFunction {
	@Override
	public List<Individual> select(double probability, Problem obj,
			Population population) {
		List<Double> fitnesses = new LinkedList<Double>();
		List<Individual> acceptedSolutions = new LinkedList<Individual>();
		for (Individual item : population) {
			Double value = obj.getFitnessValue(item);
			if (value.isInfinite())
				continue;
			acceptedSolutions.add(item);
			fitnesses.add(value);
		}
		fitnesses.sort(new Comparator<Double>() {
			@Override
			public int compare(Double first, Double second) {
				if (first < second)
					return 1;
				else if (first == second)
					return 0;
				else
					return -1;
			}
		});
		double increment = fitnesses.size() + 1;
		for (int i = 0; i < fitnesses.size(); i++) {
			increment -= 1;
			fitnesses.set(i, increment);
		}
		double sumValue = (1 + fitnesses.size()) * (fitnesses.size() / 2);

		List<Individual> result = new LinkedList<>();
		int maxItems = (int) (acceptedSolutions.size() * probability);
		if (maxItems % 2 == 1)
			maxItems -= 1;
		for (int index = 0; index < maxItems; index++) {
			double rand = random.nextDouble() * sumValue;
			int position = 0;
			double value = 0.0;
			while (true) {
				value += fitnesses.get(position);
				if (rand <= value)
					break;
				position++;
			}

			result.add(acceptedSolutions.get(position));
		}
		return result;
	}
}
