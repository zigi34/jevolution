package org.zigi.algorithm.evolution.selection;

import java.util.LinkedList;
import java.util.List;

import org.zigi.algorithm.evolution.Population;
import org.zigi.algorithm.evolution.individual.Individual;
import org.zigi.algorithm.problem.Problem;

public class RouleteWheelSelection extends SelectFunction {

	@Override
	public List<Individual> select(double probability, Problem obj,
			Population population) {
		List<Double> fitnesses = new LinkedList<Double>();
		double sumValue = 0.0;
		for (Individual item : population) {
			sumValue += obj.getFitnessValue(item);
			fitnesses.add(sumValue);
		}

		List<Individual> result = new LinkedList<>();
		int maxItems = (int) (population.size() * probability);
		if (maxItems % 2 == 1)
			maxItems -= 1;
		for (int index = 0; index < maxItems; index++) {
			double rand = random.nextDouble() * sumValue;
			int position = 0;
			while (position < fitnesses.size()
					&& fitnesses.get(position) < rand) {
				position++;
			}
			if (position == fitnesses.size())
				position -= 1;
			result.add(population.get(position));
		}
		return result;
	}

}
