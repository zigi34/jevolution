package org.zigi.algorithm.evolution.cross;

import java.security.SecureRandom;
import java.util.List;

import org.zigi.algorithm.evolution.individual.Individual;

public abstract class CrossFunction<T> {
	protected static SecureRandom random = new SecureRandom();

	public abstract void cross(List<T> input, List<T> output) throws Exception;

	public abstract List<Individual> cross(Individual first, Individual second)
			throws Exception;
}
