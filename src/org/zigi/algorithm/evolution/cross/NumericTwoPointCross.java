package org.zigi.algorithm.evolution.cross;

import java.util.LinkedList;
import java.util.List;

import org.zigi.algorithm.evolution.individual.Individual;
import org.zigi.algorithm.evolution.individual.Parameters;

public class NumericTwoPointCross extends CrossFunction<Parameters> {

	public void cross(List<Parameters> input, List<Parameters> output)
			throws Exception {
		if (input.size() % 2 != 0)
			throw new Exception("Nelze křížit lichý počet jedinců");

		for (int i = 1; i < input.size(); i += 2) {
			Parameters item1 = input.get(i - 1).createCopy();
			Parameters item2 = input.get(i).createCopy();

			int length = item1.size();
			int index1 = random.nextInt(length) + 1;
			int index2 = random.nextInt(length - index1 + 1) + index1;

			for (int j = 0; j < index1; j++) {
				Number itemObject1 = item1.get(j);
				Number itemObject2 = item2.get(j);

				item1.set(j, itemObject2);
				item2.set(j, itemObject1);
			}

			for (int j = index1; j < length; j++) {
				Number itemObject1 = item1.get(j);
				Number itemObject2 = item2.get(j);

				item1.set(j, itemObject2);
				item2.set(j, itemObject1);
			}

			output.add(item1);
			output.add(item2);
		}
	}

	@Override
	public List<Individual> cross(Individual first, Individual second)
			throws Exception {
		Parameters item1 = (Parameters) first.createCopy();
		Parameters item2 = (Parameters) second.createCopy();

		int length = item1.size();
		int index1 = random.nextInt(length) + 1;
		int index2 = random.nextInt(length - index1 + 1) + index1;

		for (int j = 0; j < index1; j++) {
			Number itemObject1 = item1.get(j);
			Number itemObject2 = item2.get(j);

			item1.set(j, itemObject2);
			item2.set(j, itemObject1);
		}

		for (int j = index1; j < length; j++) {
			Number itemObject1 = item1.get(j);
			Number itemObject2 = item2.get(j);

			item1.set(j, itemObject2);
			item2.set(j, itemObject1);
		}

		List<Individual> list = new LinkedList<Individual>();
		list.add(item1);
		list.add(item2);
		return list;
	}

}
