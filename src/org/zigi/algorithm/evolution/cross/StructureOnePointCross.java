package org.zigi.algorithm.evolution.cross;

import java.util.LinkedList;
import java.util.List;

import org.zigi.algorithm.evolution.gp.types.Expression;
import org.zigi.algorithm.evolution.individual.ExpressionTree;
import org.zigi.algorithm.evolution.individual.Individual;

public class StructureOnePointCross extends CrossFunction<ExpressionTree> {

	@Override
	public void cross(List<ExpressionTree> input, List<ExpressionTree> output)
			throws Exception {
		if (input.size() % 2 != 0)
			throw new Exception("Nelze křížit lichý počet jedinců");

		for (int i = 1; i < input.size(); i += 2) {
			ExpressionTree item1 = (ExpressionTree) input.get(i - 1)
					.createCopy();
			ExpressionTree item2 = (ExpressionTree) input.get(i).createCopy();

			int length = item1.size();
			int index1 = random.nextInt(length - 1) + 1;

			for (int j = index1; j < length; j++) {
				Expression itemObject1 = item1.get(j);
				Expression itemObject2 = item2.get(j);

				Expression prev1 = itemObject1.getParent();
				Expression prev2 = itemObject2.getParent();

				int child1 = prev1.getChild().indexOf(itemObject1);
				int child2 = prev2.getChild().indexOf(itemObject2);

				prev1.getChild().set(child1, itemObject2);
				itemObject2.setParent(prev1);

				prev2.getChild().set(child2, itemObject1);
				itemObject1.setParent(prev2);
			}

			output.add(item1);
			output.add(item2);
		}
	}

	@Override
	public List<Individual> cross(Individual first, Individual second)
			throws Exception {
		ExpressionTree item1 = (ExpressionTree) first.createCopy();
		ExpressionTree item2 = (ExpressionTree) second.createCopy();

		int length = item1.size();
		int length2 = item2.size();
		int index1 = random.nextInt(length);
		int index2 = random.nextInt(length2);

		Expression itemObject1 = item1.get(index1);
		Expression itemObject2 = item2.get(index2);

		Expression prev1 = itemObject1.getParent();
		Expression prev2 = itemObject2.getParent();

		if (prev1 != null && prev2 != null) {
			int child1 = prev1.getChild().indexOf(itemObject1);
			int child2 = prev2.getChild().indexOf(itemObject2);

			prev1.getChild().set(child1, itemObject2);
			itemObject2.setParent(prev1);

			prev2.getChild().set(child2, itemObject1);
			itemObject1.setParent(prev2);
		}
		List<Individual> list = new LinkedList<Individual>();
		list.add(item1);
		list.add(item2);
		return list;
	}
}
