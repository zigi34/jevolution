package org.zigi.algorithm.evolution.cross;

import java.util.LinkedList;
import java.util.List;

import org.zigi.algorithm.evolution.individual.Individual;
import org.zigi.algorithm.evolution.individual.Parameters;

public class NumericOnePointCross extends CrossFunction<Parameters> {
	@Override
	public void cross(List<Parameters> input, List<Parameters> output)
			throws Exception {
		if (input.size() % 2 != 0)
			throw new Exception("Cannot cross odd number individuals");

		for (int i = 1; i < input.size(); i += 2) {
			Parameters item1 = input.get(i - 1).createCopy();
			Parameters item2 = input.get(i).createCopy();

			int length = item1.size();
			int index1 = random.nextInt(length - 1) + 1;
			// int index2 = random.nextInt(length);

			for (int j = index1; j < length; j++) {
				Number itemObject1 = item1.get(j);
				Number itemObject2 = item2.get(j);

				item1.set(j, itemObject2);
				item2.set(j, itemObject1);
			}

			output.add(item1);
			output.add(item2);
		}
	}

	@Override
	public List<Individual> cross(Individual first, Individual second)
			throws Exception {
		int length = first.size();
		int index1 = random.nextInt(length - 1) + 1;

		Parameters one = (Parameters) first.createCopy();
		Parameters two = (Parameters) second.createCopy();

		for (int j = index1; j < length; j++) {
			Number itemObject1 = one.get(j);
			Number itemObject2 = two.get(j);

			one.set(j, itemObject2);
			two.set(j, itemObject1);
		}

		List<Individual> list = new LinkedList<Individual>();
		list.add(one);
		list.add(two);
		return list;
	}
}
