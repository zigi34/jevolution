package org.zigi.algorithm.problem;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.zigi.algorithm.evolution.individual.Individual;
import org.zigi.algorithm.evolution.individual.Parameters;

public abstract class PermutativeProblem extends Problem {
	private Individual bestSolution;
	protected int lo = 0;
	protected int hi = 15;
	private int dimension = hi - lo + 1;

	public static final Number FMin = 0.000001;
	public static final Number FMax = 1.0;
	public boolean isMaxBest = false;
	private Random random = new Random();

	public PermutativeProblem(String name) {
		super(name);
	}

	@Override
	public double getFitnessValue(Individual pars) {
		Parameters par = (Parameters) pars;
		return 1.0 / getTimeSpan(par);
	}

	public abstract int getTimeSpan(Parameters pars);

	public int max(int value1, int value2) {
		if (value1 > value2)
			return value1;
		else
			return value2;
	}

	public abstract double getSolutionValue(Individual pars);

	public Individual getBestSolution() {
		return bestSolution;
	}

	public void setBestSolution(Individual bestSolution) {
		this.bestSolution = bestSolution;
	}

	public int getLo() {
		return lo;
	}

	public void setLo(int lo) {
		this.lo = lo;
	}

	public int getHi() {
		return hi;
	}

	public void setHi(int hi) {
		this.hi = hi;
	}

	public int getDimension() {
		return dimension;
	}

	public void setDimension(int dimension) {
		this.dimension = dimension;
	}

	public Individual getRandomPermutation() {
		Parameters par = new Parameters();
		while (par.size() < getDimension()) {
			int value = (int) (random.nextDouble() * (getHi() + 1 - getLo()) + getLo());
			boolean isContain = false;
			for (Number num : par) {
				if (num.intValue() == value) {
					isContain = true;
					break;
				}
			}
			if (!isContain)
				par.add(value);
		}
		return par;
	}

	public boolean isValid(Individual individual) {
		List<Integer> unique = new LinkedList<Integer>();
		for (Number num : (Parameters) individual) {
			if (num.intValue() > getHi() || num.intValue() < getLo())
				return false;
			if (unique.contains(num.intValue()))
				return false;
			unique.add(num.intValue());
		}
		return true;
	}
}
