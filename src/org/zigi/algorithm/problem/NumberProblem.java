package org.zigi.algorithm.problem;

import java.util.LinkedList;
import java.util.List;

import org.zigi.algorithm.evolution.EvolutionException;
import org.zigi.algorithm.evolution.VariableRange;
import org.zigi.algorithm.evolution.individual.Individual;
import org.zigi.algorithm.evolution.individual.Parameters;
import org.zigi.algorithm.evolution.util.Constants;

public abstract class NumberProblem extends Problem {
	private Parameters bestSolution;
	private List<VariableRange<Number>> ranges = new LinkedList<VariableRange<Number>>();
	protected Number fMin = 0.0;
	protected Number fMax = 15.0;

	public static final Number FMin = 0.000001;
	public static final Number FMax = 1.0;
	public boolean isMaxBest = false;

	public NumberProblem(String name) {
		super(name);
	}

	protected abstract double getSolutionValue(Parameters pars);

	public double getFunctionValue(Parameters pars) {
		Parameters.correctParameters(pars, this);
		return getSolutionValue(pars);
	}

	/* BEST SOLUTION */
	public Parameters getBestSolution() {
		return bestSolution;
	}

	public boolean isBestSolution(Parameters pars) {
		return pars.equals(getBestSolution());
	}

	/* RANGES METHODS */
	public List<VariableRange<Number>> getRanges() {
		return ranges;
	}

	public void setRanges(List<VariableRange<Number>> ranges) {
		this.ranges = ranges;
	}

	public void addRange(VariableRange<Number> range) {
		ranges.add(range);
	}

	public void removeRange(VariableRange<Number> range) {
		ranges.remove(range);
	}

	public void removeRange(int index) {
		ranges.remove(index);
	}

	public void initialize() throws EvolutionException {
		if (getRanges().size() == 0)
			throw new EvolutionException(
					Constants.EXCEPTION_POPULATION_NOT_SET, getAlgorithm(),
					getAlgorithm().getCurrentState());
	}

	/* PARAMETERS VALUES */
	public Number getRandomValue(int index) {
		VariableRange<Number> selected = ranges.get(index);
		return selected.getRandomNumber();
	}

	/* RANDOM PARAMETERS */
	public Parameters getRandomParameters() {
		return Parameters.getRandomParameters(getRanges());
	}

	@Override
	public double getFitnessValue(Individual pars) {
		Parameters par = (Parameters) pars;
		return isMaxBest() ? getFunctionValue(par)
				: 1.0 / getFunctionValue(par);
	}

	/* FUNCTION VALS */
	public Number getfMin() {
		return fMin;
	}

	public void setfMin(Number fMin) {
		this.fMin = fMin;
	}

	public Number getfMax() {
		return fMax;
	}

	public void setfMax(Number fMax) {
		this.fMax = fMax;
	}

	/* NORMALIZE */
	public double getNormalizedFitnessValue(Parameters pars) {
		double result = ((FMax.doubleValue() - FMin.doubleValue()) / (fMin
				.doubleValue() - fMax.doubleValue()))
				* getFunctionValue(pars)
				+ ((fMin.doubleValue() * FMin.doubleValue() - fMax
						.doubleValue() * FMax.doubleValue()) / (fMin
						.doubleValue() - fMax.doubleValue()));
		return isMaxBest() ? result : 1 - result;
	}

	public boolean isMaxBest() {
		return isMaxBest;
	}

	public void setMaxBest(boolean isMaxBest) {
		this.isMaxBest = isMaxBest;
	}
}
