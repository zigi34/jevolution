package org.zigi.algorithm.problem;

import org.zigi.algorithm.evolution.EvolutionAlgorithm;
import org.zigi.algorithm.evolution.individual.Individual;

public abstract class Problem {
	private String name;
	private EvolutionAlgorithm<?> algorithm;

	/* CONSTRUCTORS */
	public Problem(String name) {
		this.name = name;
	}

	/* EVOLUTION ALGORITHM */
	public EvolutionAlgorithm<?> getAlgorithm() {
		return algorithm;
	}

	public void setAlgorithm(EvolutionAlgorithm<?> algorithm) {
		this.algorithm = algorithm;
	}

	/* PROBLEM NAME */
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/* SOLUTION */
	/**
	 * Vrac� fitness pro �e�en� p�edan� parametrem metody. Pokud je takov�
	 * �e�en� zak�zan�, vrac� hodnotu -1. Jinak vrac� kladn� hodnoty v�t�� jak
	 * 0. ��m vy��� hodnota, t�m je �e�en� lep��.
	 * 
	 * @param pars
	 *            instance konfigurace pro jedno �e�en�
	 * @return Hodnota vy��� jak 0, pokud je �e�en� platn�, jinak -1.
	 */
	public abstract double getFitnessValue(Individual pars);

	public abstract Individual getRandomIndividual();
}
