package org.zigi.algorithm.problem;

import java.util.LinkedList;
import java.util.List;

import org.zigi.algorithm.evolution.gp.types.Expression;
import org.zigi.algorithm.evolution.individual.Individual;

public abstract class StructureProblem extends Problem {

	private List<Expression> terminalList = new LinkedList<>();
	private List<Expression> neterminalList = new LinkedList<>();

	public StructureProblem(String name) {
		super(name);
	}

	public abstract double getFitnessValue(Individual pars);

	@Override
	public Individual getRandomIndividual() {
		return null;
	}

	public List<Expression> getTerminalList() {
		return terminalList;
	}

	public List<Expression> getNeterminalList() {
		return neterminalList;
	}

	public void setTerminalList(List<Expression> terminalList) {
		this.terminalList = terminalList;
	}

	public void setNeterminalList(List<Expression> neterminalList) {
		this.neterminalList = neterminalList;
	}
}
